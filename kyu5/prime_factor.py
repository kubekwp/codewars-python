from functools import reduce


def primeFactors(n):
	factor_strings = [x for x in map(lambda f: factor_to_string(f), extract_factors(n))]
	return reduce(lambda x, y: x + y, factor_strings)


def factor_to_string(f):
	return "({0})".format(f[0]) if f[1] == 1 else "({0}**{1})".format(f[0], f[1])


def extract_factors(n):
	factors = []

	curr_prime = 0
	curr_f = 2
	while n > 1:
		if n % curr_f == 0:
			n = n / curr_f
			if curr_f == curr_prime:
				factors[-1][1] += 1
			else:
				factors.append([curr_f, 1])
			curr_prime = curr_f
		else:
			curr_f += 1
	return factors