def createSpiral(N):
	if type(N) == str or N == 0:
		return []
	else:
		table = [[0 for r in range(0, N)] for c in range(0, N)]
		fill_spiral(table, 0, 0, 1, N)
		return table


def fill_spiral(table, r, c, start, n):
	if n > 0:
		start = fill_top_edge(table, r, c, start, n)
		start = fill_right_edge(table, r, c + n - 1, start, n)
		start = fill_bottom_edge(table, r + n - 1, c, start, n)
		start = fill_left_edge(table, r, c, start, n)
		fill_spiral(table, r + 1, c + 1, start + 1, n - 2)
	return start


def fill_top_edge(table, r, c, start, n):
	for cidx in range(r, r + n):
		table[r][cidx] = start
		start += 1
	return start - 1


def fill_right_edge(table, r, c, start, n):
	for ridx in range(r, r + n):
		table[ridx][c] = start
		start += 1
	return start - 1


def fill_bottom_edge(table, r, c, start, n):
	for cidx in reversed(range(c, c + n)):
		table[r][cidx] = start
		start += 1
	return start - 1


def fill_left_edge(table, r, c, start, n):
	for ridx in reversed(range(r + 1, r + n)):
		table[ridx][c] = start
		start += 1
	return start - 1