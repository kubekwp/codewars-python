def done_or_not(board):  # board[i][j]
    return 'Finished!' if check_rows(board) and check_cols(board) and check_boxes(board) else 'Try again!'


def check_rows(board):
    rows_ok = True
    for i in range(9):
        rows_ok &= check_group(board[i])
    return rows_ok


def check_cols(board):
    cols_ok = True
    for i in range(9):
        cols_ok &= check_group(get_col(board, i))
    return cols_ok


def check_boxes(board):
    boxes_ok = True
    for i in range(3):
        for j in range(3):
            boxes_ok &= check_group(get_box(board, i, j))
    return boxes_ok


def get_col(board, i):
    return [row[i] for row in board]


def get_box(board, i, j):
    box = board[i * 3][(j * 3):(j * 3 + 3)]
    box.extend(board[i * 3 + 1][(j * 3):(j * 3 + 3)])
    box.extend(board[i * 3 + 2][(j * 3):(j * 3 + 3)])
    return box


def check_group(row):
    return sum(row) == 45
