import unittest
from kyu5 import max_sum_dig


class MaxSumDigTest(unittest.TestCase):

	def test(self):
		self.assertEquals(max_sum_dig.max_sumDig(1000, 1), [1, 1000, 1000])
		self.assertEquals(max_sum_dig.max_sumDig(2000, 3), [11, 1110, 12555])
		self.assertEquals(max_sum_dig.max_sumDig(2000, 4), [21, 1120, 23665])
		self.assertEquals(max_sum_dig.max_sumDig(2000, 7), [85, 1200, 99986])
		self.assertEquals(max_sum_dig.max_sumDig(3000, 7), [141, 1600, 220756])
		self.assertEquals(max_sum_dig.max_sumDig(4000, 4), [35, 2000, 58331])

	def test_max_sum_4_digits(self):
		self.assertEquals(max_sum_dig.max_sum_4_digits(1000), 1)
		self.assertEquals(max_sum_dig.max_sum_4_digits(1002), 3)
		self.assertEquals(max_sum_dig.max_sum_4_digits(9999), 36)
		self.assertEquals(max_sum_dig.max_sum_4_digits(11112), 5)
		self.assertEquals(max_sum_dig.max_sum_4_digits(131132), 8)

	def test_closest_to_mean(self):
		self.assertEquals(max_sum_dig.closest([1000], 1000), 1000)
		self.assertEquals(max_sum_dig.closest([1, 1000], 500), 1)
		self.assertEquals(max_sum_dig.closest([1, 400, 600, 1000], 500), 400)

