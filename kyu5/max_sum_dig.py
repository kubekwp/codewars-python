def max_sumDig(nMax, maxSum):
	results = [el for el in range(1000, nMax + 1) if max_sum_4_digits(el) <= maxSum]
	return [len(results), closest(results, sum(results) / float(len(results))), sum(results)]


def max_sum_4_digits(n):
	sums = []
	while n >= 1000:
		sums.append(sum_4_digits(n % 10000))
		n = int(n / 10)
	return max(sums)


def sum_4_digits(n):
	sum4 = 0
	while n > 0:
		sum4 += n % 10
		n //= 10
	return sum4


def closest(numbers, value):
	result = numbers[0]
	for n in numbers:
		if abs(value - n) < abs(value - result):
			result = n
	return result