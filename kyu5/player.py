class Player:
	def __init__(self, cakes):
		"constructor"

	# Decide who moves first
	def firstmove(self, cakes):
		if cakes <= 2 or cakes % 4 == 2:
			return False
		return True

	# Decide your next move
	def move(self, cakes, last):
		if cakes == 3:
			return 1
		if cakes % 4 == 3 and last != 1:
			return 1
		if cakes % 4 == 0:
			if last != 2:
				return 2
			return 3
		if cakes % 4 == 1 and last != 3:
			return 3
		return 1