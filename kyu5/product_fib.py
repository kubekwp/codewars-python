def productFib(prod):
    m = 0
    m1 = 1

    while m * m1 < prod:
        temp = m1
        m1 = m + m1
        m = temp

    return [m, m1, True if m * m1 == prod else False]
