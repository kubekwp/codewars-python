from fractions import gcd
from functools import reduce

def sum_fracts(lst):
	return None if len(lst) == 0 else rational_or_int(gcd_rat(add_fracts(lst)))


def add_fracts(lst):
	return reduce((lambda r1, r2: [r1[0]*r2[1] + r1[1]*r2[0], r1[1]*r2[1]]), lst)

# def recurse_add(lst):
# 	if (len(lst) == 1):
# 		return lst[0]
# 	else:
# 		rest = recurse_add(lst[1:])
# 		return [lst[0][0] * rest[1] + rest[0] * lst[0][1],
# 				lst[0][1] * rest[1]]


def rational_or_int(rat):
	return (rat[0] / rat[1]) if (rat[0] % rat[1] == 0) else rat


def gcd_rat(rat):
	return [x // gcd(rat[0], rat[1]) for x in rat]