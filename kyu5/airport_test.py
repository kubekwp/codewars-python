import unittest
from kyu5 import airport


class AirportTest(unittest.TestCase):
	MAX = len(airport.ALPHABET)

	def test_rotate_sign(self):
		self.assertEqual("B", airport.rotate_sign("A", 1))
		self.assertEqual("C", airport.rotate_sign("A", 2))
		self.assertEqual("Z", airport.rotate_sign("A", 25))
		self.assertEqual("A", airport.rotate_sign("A", self.MAX))
		self.assertEqual("B", airport.rotate_sign("A", self.MAX + 1))

		self.assertEqual("C", airport.rotate_sign("B", 1))
		self.assertEqual("D", airport.rotate_sign("B", 2))
		self.assertEqual("Z", airport.rotate_sign("B", 24))
		self.assertEqual("B", airport.rotate_sign("B", self.MAX))
		self.assertEqual("C", airport.rotate_sign("B", self.MAX + 1))

		self.assertEqual("G", airport.rotate_sign("T", 41))


	def test_rotate_line(self):
		self.assertEqual("CAT", airport.rotate_line("CAT", [0, 0, 0]))
		self.assertEqual("DBU", airport.rotate_line("CAT", [1, 0, 0]))
		self.assertEqual("DCW", airport.rotate_line("CAT", [1, 1, 1]))
		self.assertEqual("DOG", airport.rotate_line("CAT", [1, 13, 27]))
		self.assertEqual("WORLD!", airport.rotate_line("HELLO ", (15, 49, 50, 48, 43, 13)))
		self.assertEqual("WARS", airport.rotate_line("CODE", (20, 20, 28, 0)))


	def test_accu_rotor(self):
		self.assertEqual([1], airport.accu_rotor([1]))
		self.assertEqual([1, 14], airport.accu_rotor([1, 13]))
		self.assertEqual([1, 14, 41], airport.accu_rotor([1, 13, 27]))

	def test_flap_display(self):
		self.assertEqual(["WARS"], airport.flap_display(["CODE"], [[20, 20, 28, 0]]))
		self.assertEqual(["DOG", "WARS"], airport.flap_display(["CAT", "CODE"],
															   [[1, 13, 27], [20, 20, 28, 0]]))
