import unittest
from kyu5 import best_travel


class PrimeFactorTest(unittest.TestCase):
	def test_not_enough_cities_in_list(self):
		self.assertEqual(best_travel.choose_best_sum(100, 2, [1]), None)
		self.assertEqual(best_travel.choose_best_sum(100, 3, [1, 3]), None)

	def test_combinations(self):
		self.assertEqual([[1]], best_travel.combinations(1, [1]))
		self.assertEqual([[2, 3, 4]], best_travel.combinations(3, [2, 3, 4]))
		self.assertEqual([[]], best_travel.combinations(0, [2, 3, 4]))
		self.assertEqual([[1],
						  [2]], best_travel.combinations(1, [1, 2]))
		self.assertEqual([[1, 2],
						  [1, 3],
						  [2, 3]], best_travel.combinations(2, [1, 2, 3]))

	def test(self):
		self.assertEqual(1, best_travel.choose_best_sum(100, 1, [1]))
		self.assertEqual(None, best_travel.choose_best_sum(10, 2, [10, 20, 30]))
		self.assertEqual(30, best_travel.choose_best_sum(30, 2, [10, 20, 30]))
		self.assertEqual(40, best_travel.choose_best_sum(45, 2, [10, 20, 30]))
		self.assertEqual(50, best_travel.choose_best_sum(50, 2, [10, 20, 30]))

		xs = [100, 76, 56, 44, 89, 73, 68, 56, 64, 123, 2333, 144, 50, 132, 123, 34, 89]
		self.assertEqual(230, best_travel.choose_best_sum(230, 4, xs))
		self.assertEqual(430, best_travel.choose_best_sum(430, 5, xs))
		self.assertEqual(None, best_travel.choose_best_sum(430, 8, xs))