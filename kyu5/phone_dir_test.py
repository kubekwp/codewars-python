import unittest
from kyu5 import phone_dir


class PhoneDirTest(unittest.TestCase):
    def test_not_found(self):
        book = "+1-099-500-8000 <Peter Crush> Labrador Bd."
        num = "1-421-674-8974"
        self.assertEqual(phone_dir.phone(book, num), "Error => Not found: 1-421-674-8974")

    def test_not_found2(self):
        book = "+21-099-500-8000 <Peter Crush> Labrador Bd."
        num = "1-099-500-8000"
        self.assertEqual(phone_dir.phone(book, num), "Error => Not found: 1-099-500-8000")

    def test_too_many(self):
        book = "+1-099-500-8000 <Peter Crush> Labrador Bd.\n" \
               "+1-099-500-8000 <Peter Crush> Labrador Bd."
        num = "1-099-500-8000"
        self.assertEqual(phone_dir.phone(book, num), "Error => Too many people: {0}".format(num))

    # def test_(self):
    #     book = "+1-099-500-8000 <Peter Crush> Labrador Bd.\n" \
    #            "+1-741-984-3090 <Peter Reedgrave> _Chicago"
    #     num = "1-741-984-3090"
    #     self.assertEqual(phone_dir.phone(book, num), "Phone => 1-741-984-3090, Name => Peter Reedgrave, Address => Chicago")

    def test_format_line(self):
        line = " <Peter Crush> Labrador Bd.\n"
        self.assertEqual(phone_dir.format_line(line, "1-099-500-8000"),
                         "Phone => 1-099-500-8000, Name => Peter Crush, Address => Labrador Bd.")

    def test_extract_name(self):
        line = "+1-099-500-8000 <Peter Crush> Labrador Bd."
        self.assertEqual(phone_dir.extract_name(line), "Peter Crush")

    def test_extract_addr(self):
        line = " ?Barbador:$!/ <Peter Crush> *Labrador;,_Bd."
        self.assertEqual(phone_dir.extract_addr(line), "Barbador Labrador Bd.")

    def test_all(self):
        dr = ("/+1-541-754-3010 156 Alphand_St. <J Steeve>\n "
              "133, Green, Rd. <E Kustur> NY-56423 ;+1-541-914-3010;\n"
              "+1-541-984-3012 <P Reed> /PO Box 530; Pollocksville, NC-28573\n"
              " :+1-321-512-2222 <Paul Dive> Sequoia Alley PQ-67209\n"
              "+1-741-984-3090 <Peter Reedgrave> _Chicago\n "
              ":+1-921-333-2222 <Anna Stevens> Haramburu_Street AA-67209\n"
              "+1-111-544-8973 <Peter Pan> LA\n"
              " +1-921-512-2222 <Wilfrid Stevens> Wild Street AA-67209\n"
              "<Peter Gone> LA ?+1-121-544-8974 \n"
              " <R Steell> Quora Street AB-47209 +1-481-512-2222!\n"
              "<Arthur Clarke> San Antonio $+1-121-504-8974 TT-45120\n"
              " <Ray Chandler> Teliman Pk. !+1-681-512-2222! AB-47209,\n"
              "<Sophia Loren> +1-421-674-8974 Bern TP-46017\n"
              " <Peter O'Brien> High Street +1-908-512-2222; CC-47209\n"
              "<Anastasia> +48-421-674-8974 Via Quirinal Roma\n"
              " <P Salinger> Main Street, +1-098-512-2222, Denver\n"
              "<C Powel> *+19-421-674-8974 Chateau des Fosses Strasbourg F-68000\n"
              " <Bernard Deltheil> +1-498-512-2222; Mount Av.  Eldorado\n"
              "+1-099-500-8000 <Peter Crush> Labrador Bd.\n"
              " +1-931-512-4855 <William Saurin> Bison Street CQ-23071\n"
              "<P Salinge> Main Street, +1-098-512-2222, Denve\n")

        self.assertEqual(phone_dir.phone(dr, "48-421-674-8974"),
                         "Phone => 48-421-674-8974, Name => Anastasia, Address => Via Quirinal Roma")

        self.assertEqual(phone_dir.phone(dr, "1-921-512-2222"),
                         "Phone => 1-921-512-2222, Name => Wilfrid Stevens, Address => Wild Street AA-67209")

        self.assertEqual(phone_dir.phone(dr, "1-908-512-2222"),
                         "Phone => 1-908-512-2222, Name => Peter O'Brien, Address => High Street CC-47209")

        self.assertEqual(phone_dir.phone(dr, "1-541-754-3010"),
                         "Phone => 1-541-754-3010, Name => J Steeve, Address => 156 Alphand St.")

        self.assertEqual(phone_dir.phone(dr, "1-741-984-3090"),
                         "Phone => 1-741-984-3090, Name => Peter Reedgrave, Address => Chicago")

        self.assertEqual(phone_dir.phone(dr, "1-121-504-8974"),
                "Phone => 1-121-504-8974, Name => Arthur Clarke, Address => San Antonio TT-45120")

        self.assertEqual(phone_dir.phone(dr, "1-498-512-2222"),
                "Phone => 1-498-512-2222, Name => Bernard Deltheil, Address => Mount Av. Eldorado")

        # self.assertEqual(self, phone_dir.phone(dr, "1-098-512-2222"), "Error => Too many people: 1-098-512-2222")
        # self.assertEqual(self, phone_dir.phone(dr, "5-555-555-5555"), "Error => Not found: 5-555-555-5555")
