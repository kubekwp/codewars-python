NORTH = "NORTH"
SOUTH = "SOUTH"
WEST = "WEST"
EAST = "EAST"

opposites = {NORTH: SOUTH,
			 SOUTH: NORTH,
			 WEST: EAST,
			 EAST: WEST}


def dirReduc(arr):
	stack = []
	for dir in arr:
		if not stack:
			stack.append(dir)
		else:
			stack_top = stack.pop()
			if not is_opposite(stack_top, dir):
				stack.append(stack_top)
				stack.append(dir)
	return stack


def is_opposite(dir1, dir2):
	return opposites[dir1] == dir2
