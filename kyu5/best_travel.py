def choose_best_sum(t, k, ls):
	if not type(t) == int:
		return None
	if len(ls) < k:
		return None

	combs = combinationss(k, ls)
	sums = sorted([sum(comb) for comb in combs if sum(comb) <= t], reverse=True)
	return sums[0] if len(sums) > 0 else None


def combinationss(k, ls):
	if len(ls) == k:
		return [ls]
	if k == 0:
		return [[]]

	return [[ls[0]] + comb for comb in combinationss(k-1, ls[1:])] + combinationss(k, ls[1:])
