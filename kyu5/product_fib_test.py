import unittest
from kyu5 import product_fib

class ProductFibTest(unittest.TestCase):

    def test(self):
        self.assertEqual(product_fib.productFib(0), [0 ,1, True])
        self.assertEqual(product_fib.productFib(1), [1 ,1, True])
        self.assertEqual(product_fib.productFib(2), [1 ,2, True])
        self.assertEqual(product_fib.productFib(3), [2 ,3, False])
        self.assertEqual(product_fib.productFib(4895), [55, 89, True])
        self.assertEqual(product_fib.productFib(5895), [89, 144, False])