from bs4 import BeautifulSoup
import urllib.request

Url = 'https://www.codewars.com/users/leaderboard'


class Leaderboard():
	def __init__(self, users):
		self.position = users


class User():
	def __init__(self, place, name, clan, honor):
		self.place = place
		self.name = name
		self.clan = clan
		self.honor = honor


def row_to_user(row):
	tr = (BeautifulSoup(str(row))).find("tr")
	cells = tr.find_all("td")
	place = int(cells[0].text.replace("#", ""))
	name = tr["data-username"]
	clan = cells[2].text.strip()
	honor = int(cells[3].text)
	return User(place, name, clan, honor)


def solution():
	soup = BeautifulSoup(urllib.request.urlopen(Url).read())
	# resp_file = open("response.txt", "r", encoding="UTF-8")
	# soup = BeautifulSoup(resp_file.read())
	tab_rows = soup.select(".leaderboard")[0].table.find_all("tr")
	return Leaderboard({user.place: user for user in map(row_to_user, tab_rows[1:])})
