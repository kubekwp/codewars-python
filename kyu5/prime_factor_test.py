import unittest
from kyu5 import prime_factor


class PrimeFactorTest(unittest.TestCase):

	def test_prime_factors(self):
		self.assertEqual(prime_factor.primeFactors(2), "(2)")
		self.assertEqual(prime_factor.primeFactors(3), "(3)")
		self.assertEqual(prime_factor.primeFactors(4), "(2**2)")
		self.assertEqual(prime_factor.primeFactors(7775460), "(2**2)(3**3)(5)(7)(11**2)(17)")

	def test_extract_factors(self):
		self.assertEqual(prime_factor.extract_factors(2), [[2, 1]])
		self.assertEqual(prime_factor.extract_factors(3), [[3, 1]])
		self.assertEqual(prime_factor.extract_factors(5), [[5, 1]])
		self.assertEqual(prime_factor.extract_factors(17), [[17, 1]])
		self.assertEqual(prime_factor.extract_factors(4), [[2, 2]])
		self.assertEqual(prime_factor.extract_factors(6), [[2, 1], [3, 1]])
		self.assertEqual(prime_factor.extract_factors(8), [[2, 3]])
		self.assertEqual(prime_factor.extract_factors(7775460),
						 [[2, 2], [3, 3], [5, 1], [7, 1], [11, 2], [17, 1]])


	def test_factor_to_string(self):
		self.assertEqual(prime_factor.factor_to_string([3, 2]), "(3**2)")
		self.assertEqual(prime_factor.factor_to_string([3, 1]), "(3)")