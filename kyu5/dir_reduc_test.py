from kyu5 import dir_reduc
import unittest

NORTH = "NORTH"
SOUTH = "SOUTH"
WEST = "WEST"
EAST = "EAST"


class DirReducTest(unittest.TestCase):
	def test_one_step(self):
		self.assertEqual([NORTH], dir_reduc.dirReduc([NORTH]))
		self.assertEqual([SOUTH], dir_reduc.dirReduc([SOUTH]))
		self.assertEqual([WEST], dir_reduc.dirReduc([WEST]))
		self.assertEqual([EAST], dir_reduc.dirReduc([EAST]))

	def test_two_steps_no_reduce(self):
		self.assertEqual([NORTH, NORTH], dir_reduc.dirReduc([NORTH, NORTH]))
		self.assertEqual([SOUTH, SOUTH], dir_reduc.dirReduc([SOUTH, SOUTH]))
		self.assertEqual([WEST, WEST], dir_reduc.dirReduc([WEST, WEST]))
		self.assertEqual([EAST, EAST], dir_reduc.dirReduc([EAST, EAST]))

	def test_two_steps_reduce(self):
		self.assertEqual([], dir_reduc.dirReduc([NORTH, SOUTH]))
		self.assertEqual([], dir_reduc.dirReduc([SOUTH, NORTH]))
		self.assertEqual([], dir_reduc.dirReduc([WEST, EAST]))
		self.assertEqual([], dir_reduc.dirReduc([EAST, WEST]))

	def test_three_steps_reduce(self):
		self.assertEqual([SOUTH], dir_reduc.dirReduc([NORTH, SOUTH, SOUTH]))
		self.assertEqual([WEST], dir_reduc.dirReduc([SOUTH, NORTH, WEST]))
		self.assertEqual([EAST], dir_reduc.dirReduc([WEST, EAST, EAST]))
		self.assertEqual([SOUTH], dir_reduc.dirReduc([EAST, WEST, SOUTH]))

		self.assertEqual([SOUTH], dir_reduc.dirReduc([SOUTH, SOUTH, NORTH]))
		self.assertEqual([NORTH], dir_reduc.dirReduc([NORTH, EAST, WEST]))

	def test_multiple_steps(self):
		self.assertEqual([WEST],
						 dir_reduc.dirReduc(["NORTH", "SOUTH", "SOUTH", "EAST", "WEST", "NORTH", "WEST"]))
		self.assertEqual(["NORTH", "WEST", "SOUTH", "EAST"],
						 dir_reduc.dirReduc(["NORTH", "WEST", "SOUTH", "EAST"]))


	def test_is_opposite(self):
		self.assertTrue(dir_reduc.is_opposite(NORTH, SOUTH))
		self.assertTrue(dir_reduc.is_opposite(SOUTH, NORTH))
		self.assertTrue(dir_reduc.is_opposite(WEST, EAST))
		self.assertTrue(dir_reduc.is_opposite(EAST, WEST))

		self.assertFalse(dir_reduc.is_opposite(NORTH, NORTH))
		self.assertFalse(dir_reduc.is_opposite(SOUTH, SOUTH))
		self.assertFalse(dir_reduc.is_opposite(EAST, EAST))
		self.assertFalse(dir_reduc.is_opposite(WEST, WEST))