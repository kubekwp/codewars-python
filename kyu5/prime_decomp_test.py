import unittest
from kyu5 import prime_decomp


class PrimeDecompTest(unittest.TestCase):
	def test_all_prime_factors(self):
		self.assertEqual(prime_decomp.getAllPrimeFactors("a"), [])
		self.assertEqual(prime_decomp.getAllPrimeFactors(1.2), [])
		self.assertEqual(prime_decomp.getAllPrimeFactors(-1), [])
		self.assertEqual(prime_decomp.getAllPrimeFactors(0), [])

		self.assertEqual(prime_decomp.getAllPrimeFactors(1), [1])
		self.assertEqual(prime_decomp.getAllPrimeFactors(2), [2])
		self.assertEqual(prime_decomp.getAllPrimeFactors(3), [3])
		self.assertEqual(prime_decomp.getAllPrimeFactors(4), [2, 2])
		self.assertEqual(prime_decomp.getAllPrimeFactors(17), [17])
		self.assertEqual(prime_decomp.getAllPrimeFactors(100), [2, 2, 5, 5])

	def test_unique_prime_factors_with_count(self):
		self.assertEqual(prime_decomp.getUniquePrimeFactorsWithCount("a"), [[], []])
		self.assertEqual(prime_decomp.getUniquePrimeFactorsWithCount(1.2), [[], []])
		self.assertEqual(prime_decomp.getUniquePrimeFactorsWithCount(-1), [[], []])
		self.assertEqual(prime_decomp.getUniquePrimeFactorsWithCount(0), [[], []])

		self.assertEqual(prime_decomp.getUniquePrimeFactorsWithCount(1), [[1], [1]])
		self.assertEqual(prime_decomp.getUniquePrimeFactorsWithCount(2), [[2], [1]])
		self.assertEqual(prime_decomp.getUniquePrimeFactorsWithCount(3), [[3], [1]])
		self.assertEqual(prime_decomp.getUniquePrimeFactorsWithCount(4), [[2], [2]])
		self.assertEqual(prime_decomp.getUniquePrimeFactorsWithCount(17), [[17], [1]])
		self.assertEqual(prime_decomp.getUniquePrimeFactorsWithCount(100), [[2, 5], [2, 2]])

	def test_unique_prime_factors_with_products(self):
		self.assertEqual(prime_decomp.getUniquePrimeFactorsWithProducts("a"), [])
		self.assertEqual(prime_decomp.getUniquePrimeFactorsWithProducts(1.2), [])
		self.assertEqual(prime_decomp.getUniquePrimeFactorsWithProducts(-1), [])
		self.assertEqual(prime_decomp.getUniquePrimeFactorsWithProducts(0), [])

		self.assertEqual(prime_decomp.getUniquePrimeFactorsWithProducts(1), [1])
		self.assertEqual(prime_decomp.getUniquePrimeFactorsWithProducts(2), [2])
		self.assertEqual(prime_decomp.getUniquePrimeFactorsWithProducts(3), [3])
		self.assertEqual(prime_decomp.getUniquePrimeFactorsWithProducts(4), [4])
		self.assertEqual(prime_decomp.getUniquePrimeFactorsWithProducts(17), [17])
		self.assertEqual(prime_decomp.getUniquePrimeFactorsWithProducts(100), [4, 25])


	def test_unique_factors_reduce(self):
		self.assertEqual(prime_decomp.unique_factors_reduce([2]), [[2, 1]])
		self.assertEqual(prime_decomp.unique_factors_reduce([2, 2]), [[2, 2]])
		self.assertEqual(prime_decomp.unique_factors_reduce([2, 2, 2]), [[2, 3]])
		self.assertEqual(prime_decomp.unique_factors_reduce([2, 2, 2, 3]), [[2, 3], [3, 1]])
		self.assertEqual(prime_decomp.unique_factors_reduce([2, 2, 2, 3, 3, 5]), [[2, 3], [3, 2], [5, 1]])

	def test_append_count(self):
		self.assertEqual(prime_decomp.append_count([2, 1], [[3, 2], [4, 3]]), [[2, 1], [3, 2], [4, 3]])
		self.assertEqual(prime_decomp.append_count([2, 1], [[2, 2], [4, 3]]), [[2, 3], [4, 3]])

	def test_elements_at_index(self):
		self.assertEqual(prime_decomp.elements_at_index([[2, 1], [3, 2], [4, 3]], 0), [2, 3, 4])
		self.assertEqual(prime_decomp.elements_at_index([[2, 1], [3, 2], [4, 3]], 1), [1, 2, 3])