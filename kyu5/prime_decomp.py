def getAllPrimeFactors(n):
	if type(n) is not int or n <= 0:
		return []
	if n == 1:
		return [n]

	factors = []
	cur_factor = 2
	while n > 1:
		if n % cur_factor == 0:
			factors.append(cur_factor)
			n = n / cur_factor
		else:
			cur_factor += 1
	return factors


def getUniquePrimeFactorsWithCount(n):
	if type(n) is not int or n <= 0:
		return [[], []]
	if n == 1:
		return [[1], [1]]

	factors_with_count = unique_factors_reduce(getAllPrimeFactors(n))
	return [elements_at_index(factors_with_count, 0),
			elements_at_index(factors_with_count, 1)]


def getUniquePrimeFactorsWithProducts(n):
	if type(n) is not int or n <= 0:
		return []
	if n == 1:
		return [n]

	return [x[0] ** x[1] for x in (unique_factors_reduce(getAllPrimeFactors(n)))]


def elements_at_index(elements, idx):
	return [x[idx] for x in elements]


def unique_factors_reduce(factors):
	if len(factors) == 1:
		return [[factors[0], 1]]

	return append_count([factors[0], 1], unique_factors_reduce(factors[1:]))


def append_count(first, rest):
	if first[0] == rest[0][0]:
		return [[first[0], first[1] + rest[0][1]]] + rest[1:]
	return [first] + rest
