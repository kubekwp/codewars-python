ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ ?!@#&()|<>.:=-+*/0123456789"


def rotate_sign(sign, rot):
	return ALPHABET[(ALPHABET.index(sign) + rot) % len(ALPHABET)]


def accu_rotor(rotor):
	accu = []
	for i in range(0, len(rotor)):
		if i == 0:
			accu.append(rotor[i])
		else:
			accu.append(rotor[i] + accu[i - 1])
	return accu


def rotate_line(line, rotor):
	return "".join(rotate_sign(x[0], x[1]) for x in list(zip(line, accu_rotor(rotor))))


def flap_display(lines, rotors):
	rotated = []
	for i in range(0,len(lines)):
		rotated.append(rotate_line(lines[i], rotors[i]))
	return rotated