import re

def phone(strng, num):
    lines = [l for l in filter(lambda line: ("+" + num) in line, strng.split("\n"))]

    if len(lines) == 0:
        return "Error => Not found: {0}".format(num)
    elif len(lines) > 1:
        return "Error => Too many people: {0}".format(num)
    else:
        return format_line(without_num(lines[0], num), num)


def without_num(line, num):
    return line.replace("+" + num, "")


def format_line(line, num):
    return "Phone => {0}, Name => {1}, Address => {2}".format(num, extract_name(line), extract_addr(line))


def extract_name(line):
    return line.split("<")[1].split(">")[0]


def extract_addr(line):
    addr = line.split("<")[0] + line.split(">")[1]
    return re.sub("  ", " ", re.sub("_", " ", re.sub(":|\*|!|/|\?|,|;|\$", "", addr))).strip()
