import unittest
from kyu5 import scrap_500


class Scrap500Test(unittest.TestCase):
	def test(self):
		leaderboard = scrap_500.solution()

		self.assertEqual(500, len(leaderboard.position))

		self.assertEqual(leaderboard.position[1].name, 'g964')
		self.assertEqual(leaderboard.position[2].name, 'myjinxin2015')
		self.assertEqual(leaderboard.position[3].name, 'SteffenVogel_79')
		self.assertEqual(leaderboard.position[4].name, 'smile67')
		self.assertEqual(leaderboard.position[5].name, 'GiacomoSorbi')

		self.assertEqual(leaderboard.position[1].clan, 'None')
		self.assertEqual(leaderboard.position[2].clan, 'China Changyuan')
		self.assertEqual(leaderboard.position[3].clan, 'CSV - SLayer')
		self.assertEqual(leaderboard.position[4].clan, 'PropertyExpert, Germany')
		self.assertEqual(leaderboard.position[5].clan, '')
		
		self.assertEqual(leaderboard.position[1].honor > 40000, True)
		self.assertEqual(leaderboard.position[2].honor > 20000, True)
		self.assertEqual(leaderboard.position[3].honor > 20000, True)
		self.assertEqual(leaderboard.position[4].honor > 15000, True)
		self.assertEqual(leaderboard.position[5].honor > 17000, True)

	def test_row_to_user(self):
		user = scrap_500.row_to_user(
			"<tr data-username=\"g964\"><td class=\"rank is-small\">#1</td><td class=\"is-big\"><div class=\"small-hex is-extra-wide is-purple-rank is-left mtx mrl\"><div class=\"inner-small-hex is-extra-wide \"><span>1 kyu</span></div></div><a href=\"/users/g964\"><img alt=\"Profile-pic\" class=\"profile-pic\" src=\"/assets/profile-pic-649c13c1c5baf0885b052e59e46c11c7.png\"/>g964</a></td><td>None</td><td>100542</td></tr>")
		self.assertEqual(user.place, 1)
		self.assertEqual(user.name, 'g964')
		self.assertEqual(user.clan, 'None')
		self.assertTrue(user.honor > 40000)
