import unittest
from kyu5 import spiral


class SpiralTest(unittest.TestCase):
	def test_empty(self):
		self.assertEqual(spiral.createSpiral(0), [])

	def test_fill_spiral_1(self):
		table = [[0, 0, 0],
				 [0, 0, 0],
				 [0, 0, 0]]

		spiral.fill_spiral(table, 1, 1, 1, 1)

		self.assertEqual(table, [
			[0, 0, 0],
			[0, 1, 0],
			[0, 0, 0]])

	def test_fill_spiral_2(self):
		table = [[0, 0],
				 [0, 0]]

		start = spiral.fill_spiral(table, 0, 0, 1, 2)

		self.assertEqual(start, 4)
		self.assertEqual(table, [
			[1, 2],
			[4, 3]])

	def test_fill_spiral_3(self):
		table = [[0, 0, 0],
				 [0, 0, 0],
				 [0, 0, 0]]

		spiral.fill_spiral(table, 0, 0, 1, 3)

		self.assertEqual(table, [
			[1, 2, 3],
			[8, 9, 4],
			[7, 6, 5]])

	def test_fill_spiral_3_insde(self):
		table = [[0, 0, 0, 0, 0],
				 [0, 0, 0, 0, 0],
				 [0, 0, 0, 0, 0],
				 [0, 0, 0, 0, 0],
				 [0, 0, 0, 0, 0]]

		spiral.fill_spiral(table, 1, 1, 1, 3)

		self.assertEqual(table, [
			[0, 0, 0, 0, 0],
			[0, 1, 2, 3, 0],
			[0, 8, 9, 4, 0],
			[0, 7, 6, 5, 0],
			[0, 0, 0, 0, 0]])

	def test_create_spiral_4(self):
		self.assertEqual(spiral.createSpiral(4), [
			[1, 2, 3, 4],
			[12, 13, 14, 5],
			[11, 16, 15, 6],
			[10, 9, 8, 7]])

	def test_create_spiral_5(self):
		self.assertEqual(spiral.createSpiral(5), [
			[1, 2, 3, 4, 5],
			[16, 17, 18, 19, 6],
			[15, 24, 25, 20, 7],
			[14, 23, 22, 21, 8],
			[13, 12, 11, 10, 9]])

	def test_fill_top_edge_2(self):
		table = [[0, 0],
				 [0, 0]]

		start = spiral.fill_top_edge(table, 0, 0, 1, 2)

		self.assertEqual(start, 2)
		self.assertEqual(table, [
			[1, 2],
			[0, 0]])

	def test_fill_top_edge_3(self):
		table = [[0, 0, 0],
				 [0, 0, 0],
				 [0, 0, 0]]

		start = spiral.fill_top_edge(table, 0, 0, 1, 3)

		self.assertEqual(start, 3)
		self.assertEqual(table, [
			[1, 2, 3],
			[0, 0, 0],
			[0, 0, 0]])

	def test_fill_top_edge_3_inside(self):
		table = [[0, 0, 0, 0, 0],
				 [0, 0, 0, 0, 0],
				 [0, 0, 0, 0, 0],
				 [0, 0, 0, 0, 0],
				 [0, 0, 0, 0, 0]]

		start = spiral.fill_top_edge(table, 1, 1, 1, 3)

		self.assertEqual(start, 3)
		self.assertEqual(table, [
			[0, 0, 0, 0, 0],
			[0, 1, 2, 3, 0],
			[0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0]])

	def test_fill_right_edge_2(self):
		table = [[0, 0],
				 [0, 0]]

		start = spiral.fill_right_edge(table, 0, 1, 1, 2)

		self.assertEqual(start, 2)
		self.assertEqual(table, [
			[0, 1],
			[0, 2]])

	def test_fill_right_edge_3(self):
		table = [[0, 0, 0],
				 [0, 0, 0],
				 [0, 0, 0]]

		start = spiral.fill_right_edge(table, 0, 2, 1, 3)

		self.assertEqual(start, 3)
		self.assertEqual(table, [
			[0, 0, 1],
			[0, 0, 2],
			[0, 0, 3]])

	def test_fill_right_edge_3_inside(self):
		table = [[0, 0, 0, 0, 0],
				 [0, 0, 0, 0, 0],
				 [0, 0, 0, 0, 0],
				 [0, 0, 0, 0, 0],
				 [0, 0, 0, 0, 0]]

		start = spiral.fill_right_edge(table, 1, 3, 1, 3)

		self.assertEqual(start, 3)
		self.assertEqual(table, [
			[0, 0, 0, 0, 0],
			[0, 0, 0, 1, 0],
			[0, 0, 0, 2, 0],
			[0, 0, 0, 3, 0],
			[0, 0, 0, 0, 0]])

	def test_fill_bottom_edge_2(self):
		table = [[0, 0],
				 [0, 0]]

		start = spiral.fill_bottom_edge(table, 1, 0, 1, 2)

		self.assertEqual(start, 2)
		self.assertEqual(table, [
			[0, 0],
			[2, 1]])

	def test_fill_bottom_edge_3(self):
		table = [[0, 0, 0],
				 [0, 0, 0],
				 [0, 0, 0]]

		start = spiral.fill_bottom_edge(table, 2, 0, 1, 3)

		self.assertEqual(start, 3)
		self.assertEqual(table, [
			[0, 0, 0],
			[0, 0, 0],
			[3, 2, 1]])

	def test_fill_right_edge_3_inside(self):
		table = [[0, 0, 0, 0, 0],
				 [0, 0, 0, 0, 0],
				 [0, 0, 0, 0, 0],
				 [0, 0, 0, 0, 0],
				 [0, 0, 0, 0, 0]]

		start = spiral.fill_bottom_edge(table, 3, 1, 1, 3)

		self.assertEqual(start, 3)
		self.assertEqual(table, [
			[0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0],
			[0, 3, 2, 1, 0],
			[0, 0, 0, 0, 0]])

	def test_fill_left_edge_2(self):
		table = [[0, 0],
				 [0, 0]]

		start = spiral.fill_left_edge(table, 0, 0, 1, 2)

		self.assertEqual(start, 1)
		self.assertEqual(table, [
			[0, 0],
			[1, 0]])

	def test_fill_left_edge_3(self):
		table = [[0, 0, 0],
				 [0, 0, 0],
				 [0, 0, 0]]

		start = spiral.fill_left_edge(table, 0, 0, 1, 3)

		self.assertEqual(start, 2)
		self.assertEqual(table, [
			[0, 0, 0],
			[2, 0, 0],
			[1, 0, 0]])

	def test_fill_left_edge_3_inside(self):
		table = [[0, 0, 0, 0, 0],
				 [0, 0, 0, 0, 0],
				 [0, 0, 0, 0, 0],
				 [0, 0, 0, 0, 0],
				 [0, 0, 0, 0, 0]]

		start = spiral.fill_left_edge(table, 1, 1, 1, 3)

		self.assertEqual(start, 2)
		self.assertEqual(table, [
			[0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0],
			[0, 2, 0, 0, 0],
			[0, 1, 0, 0, 0],
			[0, 0, 0, 0, 0]])
