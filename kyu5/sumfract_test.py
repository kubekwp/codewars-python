import unittest
from kyu5 import sumfracts


class SumfractTest(unittest.TestCase):

	def test_sum_fract(self):
		self.assertEqual(sumfracts.sum_fracts([]), None)
		self.assertEqual(sumfracts.sum_fracts([[1, 2]]), [1, 2])
		self.assertEqual(sumfracts.sum_fracts([[4, 2]]), 2)
		self.assertEqual(sumfracts.sum_fracts([[1, 3], [1, 3]]), [2, 3])
		self.assertEqual(sumfracts.sum_fracts([[1, 2], [1, 3], [1, 4]]), [13, 12])
