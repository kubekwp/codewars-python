import unittest
from kyu6 import bankers_plan
from random import randint
from math import floor


class BankersPlanTest(unittest.TestCase):
	def test(self):
		self.assertEquals(bankers_plan.fortune(100000, 1, 9185, 12, 1), False)
		self.assertEquals(bankers_plan.fortune(100000000, 1, 100000, 50, 1), True)
		self.assertEquals(bankers_plan.fortune(100000000, 1.5, 10000000, 50, 1), False)
		self.assertEquals(bankers_plan.fortune(100000000, 5, 1000000, 50, 1), True)
		self.assertEquals(bankers_plan.fortune(14253332, 1, 950688, 12, 6), True)

	def fortune_sol(self, f0, p, c0, n, i):
		prev_x, prev_c = f0, c0
		for k in range(1, n):
			nou_x = floor(prev_x + p/100. * prev_x - prev_c)
			nou_c = floor(prev_c + i/100. * prev_c)
			prev_x, prev_c = nou_x, nou_c
		return nou_x >= 0

	# def test_random(self):
	# 	print("Random tests ****************** ")
	# 	for _ in range(0, 200):
	# 		f0 = randint(100000, 15000000)
	# 		p = randint(1, 10)
	# 		c0 = int(f0 / 15.0 + randint(100, 1500))
	# 		n = randint(10, 25)
	# 		j = randint(1, 8)
	# 		print("f0: {0}, p: {1}, c0: {2}, n: {3}, i: {4}".format(f0, p, c0, n, j))
	# 		print("bankers plan: {0}".format(bankers_plan.fortune(f0, p, c0, n, j)))
	# 		print("solution: {0}".format(self.fortune_sol(f0, p, c0, n, j)))
	# 		self.assertEquals(bankers_plan.fortune(f0, p, c0, n, j), self.fortune_sol(f0, p, c0, n, j))
