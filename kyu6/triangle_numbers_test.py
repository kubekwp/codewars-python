import unittest
from kyu6 import triangle_number

class TriangleNumberTest(unittest.TestCase):

	def test(self):
		self.assertFalse(triangle_number.is_triangle_number(2))
		self.assertFalse(triangle_number.is_triangle_number(6.15))

		self.assertTrue(triangle_number.is_triangle_number(0))
		self.assertTrue(triangle_number.is_triangle_number(3))
		self.assertTrue(triangle_number.is_triangle_number(6))
		self.assertTrue(triangle_number.is_triangle_number(10))
		self.assertTrue(triangle_number.is_triangle_number(15))
