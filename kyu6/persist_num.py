def persistence(n):
    count = 0
    while n > 9:
        m = n
        n = 1
        while m > 0:
            n *= (m % 10)
            m = int(m / 10)
        count += 1
    return count
