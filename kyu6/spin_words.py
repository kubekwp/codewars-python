def spin_words(sentence):
	return " ".join([(word[::-1] if len(word) > 4 else word) for word in sentence.split(" ")])

# def spin_words(sentence):
# 	return " ".join([spin_word(word) for word in sentence.split(" ")])
#
# def spin_word(word):
# 	return word[::-1] if len(word) > 4 else word
