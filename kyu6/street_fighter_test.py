import unittest

from kyu6 import street_fighter as sf


fighters = [
	["Ryu", "E.Honda", "Blanka", "Guile", "Balrog", "Vega"],
	["Ken", "Chun Li", "Zangief", "Dhalsim", "Sagat", "M.Bison"]
]
opts = ["up", "down", "right", "left"]


class StreetFighterTest(unittest.TestCase):

	def test_no_moves(self):
		moves = []
		solution = []
		self.assertEquals(sf.street_fighter_selection(fighters, (0, 0), moves), solution)

	def test_move_down(self):
		moves = ["down"]
		solution = ["Ken"]
		self.assertEquals(sf.street_fighter_selection(fighters, (0, 0), moves), solution)

	def test_no_move_down_when_exceed_bottom_border(self):
		moves = ["down"]
		solution = ["Ken"]
		self.assertEquals(sf.street_fighter_selection(fighters, (1, 0), moves), solution)

	def test_move_down_more_than_one_place(self):
		moves = ["down", "down"]
		solution = ["Ken", "Ken"]
		self.assertEquals(sf.street_fighter_selection(fighters, (0, 0), moves), solution)

	def test_move_up(self):
		moves = ["up"]
		solution = ["Ryu"]
		self.assertEquals(sf.street_fighter_selection(fighters, (1, 0), moves), solution)

	def test_no_move_up_when_exceed_top_border(self):
		moves = ["up"]
		solution = ["Ryu"]
		self.assertEquals(sf.street_fighter_selection(fighters, (0, 0), moves), solution)

	def test_move_up_more_than_one_place(self):
		moves = ["up", "up"]
		solution = ["Ryu", "Ryu"]
		self.assertEquals(sf.street_fighter_selection(fighters, (1, 0), moves), solution)

	def test_move_right(self):
		moves = ["right"]
		solution = ["E.Honda"]
		self.assertEquals(sf.street_fighter_selection(fighters, (0, 0), moves), solution)

	def test_move_right_more_than_one_place(self):
		moves = ["right", "right"]
		solution = ["E.Honda", "Blanka"]
		self.assertEquals(sf.street_fighter_selection(fighters, (0, 0), moves), solution)

	def test_move_right_to_leftmost_when_exceed_border(self):
		moves = ["right"]
		solution = ["Ryu"]
		self.assertEquals(sf.street_fighter_selection(fighters, (0, 5), moves), solution)

	def test_move_left(self):
		moves = ["left"]
		solution = ["Ryu"]
		self.assertEquals(sf.street_fighter_selection(fighters, (0, 1), moves), solution)

	def test_move_left_more_than_one_place(self):
		moves = ["left", "left"]
		solution = ["E.Honda", "Ryu"]
		self.assertEquals(sf.street_fighter_selection(fighters, (0, 2), moves), solution)

	def test_move_left_to_rightmost_when_exceed_border(self):
		moves = ["left", "left"]
		solution = ["Vega", "Balrog"]
		self.assertEquals(sf.street_fighter_selection(fighters, (0, 0), moves), solution)

	def test_move_left_8_times(self):
		moves = ["left"] * 8
		solution = ['Vega', 'Balrog', 'Guile', 'Blanka', 'E.Honda', 'Ryu', 'Vega', 'Balrog']
		self.assertEquals(sf.street_fighter_selection(fighters, (0, 0), moves), solution)

	def test_move_right_8_times(self):
		moves = ["right"] * 8
		solution = ['E.Honda', 'Blanka', 'Guile', 'Balrog', 'Vega', 'Ryu', 'E.Honda', 'Blanka']
		self.assertEquals(sf.street_fighter_selection(fighters, (0, 0), moves), solution)

	def test_move_up_4_times(self):
		moves = ["up"] * 4
		solution = ['Ryu', 'Ryu', 'Ryu', 'Ryu']
		self.assertEquals(sf.street_fighter_selection(fighters, (0, 0), moves), solution)

	def test_move_down_4_times(self):
		moves = ["down"] * 4
		solution = ["Ken", "Ken", "Ken", "Ken"]
		self.assertEquals(sf.street_fighter_selection(fighters, (0, 0), moves), solution)

	def test_all_4_directions(self):
		moves = ["up","left","down","right"]*2
		solution = ['Ryu', 'Vega', 'M.Bison', 'Ken', 'Ryu', 'Vega', 'M.Bison', 'Ken']
		self.assertEquals(sf.street_fighter_selection(fighters, (0, 0), moves), solution)

	def test_cover_all_characters(self):
		moves = ["up"]+["left"]*6+["down"]+["right"]*6
		solution = ['Ryu', 'Vega', 'Balrog', 'Guile', 'Blanka', 'E.Honda', 'Ryu', 'Ken', 'Chun Li', 'Zangief', 'Dhalsim', 'Sagat', 'M.Bison', 'Ken']
		self.assertEquals(sf.street_fighter_selection(fighters, (0, 0), moves), solution)
