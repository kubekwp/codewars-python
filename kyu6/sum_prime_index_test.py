import unittest
from kyu6.sum_prime_index import total


class SumPrimeIndexTest(unittest.TestCase):
    def test(self):
        self.assertEqual(total([]), 0)
        self.assertEqual(total([1]), 0)
        self.assertEqual(total([1, 2]), 0)
        self.assertEqual(total([1, 2, 3]), 3)
        self.assertEqual(total([1, 2, 3, 4]), 7)
        self.assertEqual(total([1, 2, 3, 4, 5, 6]), 13)
        self.assertEqual(total([1, 2, 3, 4, 5, 6, 7, 8]), 21)
        self.assertEqual(total([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]), 21)
        self.assertEqual(total([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]), 33)
        self.assertEqual(total([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]), 47)