import math

def fortune(f0, p, c0, n, i):
	fk = f0
	ck = c0
	for k in range(1, n + 1):
		fk = (int)(fk + p / 100 * fk - ck)
		ck = (int)(ck + i / 100 * ck)
	return fk >= 0
