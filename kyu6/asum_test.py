import unittest
from kyu6 import asum


class ASumTest(unittest.TestCase):

	def test(self):
		self.assertEquals(asum.find_nb(1), 1)
		self.assertEquals(asum.find_nb(2), -1)
		self.assertEquals(asum.find_nb(4183059834009), 2022)
		self.assertEquals(asum.find_nb(24723578342962), -1)
		self.assertEquals(asum.find_nb(135440716410000), 4824)
		self.assertEquals(asum.find_nb(40539911473216), 3568)
		self.assertEquals(asum.find_nb(26825883955641), 3218)
