import math


def to_meter_per_sec(v0):
	return v0 * 1000 / 3600


def height(v, t):
	return v * t - 0.5 * 9.81 * t * t


def max_ball(v0):
	if v0 == 0:
		return 0

	v = to_meter_per_sec(v0)

	tmax = v / 9.81
	tmax_down = math.floor(tmax * 10) / 10
	tmax_up = math.ceil(tmax * 10) / 10

	return tmax_down * 10 if height(v, tmax_down) > height(v, tmax_up) else tmax_up * 10
