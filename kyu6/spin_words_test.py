import unittest
from kyu6 import spin_words

class SpinWordsTest(unittest.TestCase):

	def test_empty(self):
		self.assertEqual(spin_words.spin_words(""), "")

	def test_single_short_word_not_reversed(self):
		self.assertEqual(spin_words.spin_words("test"), "test")

	def test_single_long_word_reversed(self):
		self.assertEqual(spin_words.spin_words("Qbology"), "ygolobQ")

	def test_multiple_short_words(self):
		self.assertEqual(spin_words.spin_words("test dupa test"), "test dupa test")

	def test_multiple_long_words(self):
		self.assertEqual(spin_words.spin_words("test Qbology test Jakub"), "test ygolobQ test bukaJ")
