def sumDig_nthTerm(initVal, patternL, nthTerm):
    term = initVal
    for i in range(0, nthTerm - 1):
        term += patternL[i % len(patternL)]

    sum = 0
    while term > 0:
        sum += term % 10
        term //= 10
    return sum
