import math


def is_prime(i):
    for j in range(2, int(math.sqrt(i)) + 1):
        if i % j == 0:
            return False
    return i >= 2


def total(arr):
    return sum([arr[i] for i in range(len(arr)) if is_prime(i)])
