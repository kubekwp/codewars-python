import unittest
from kyu6 import updown


class Updown(unittest.TestCase):

	def test_arrange(self):
		self.assertEqual(updown.arrange("who hit retaining The That a we taken"),
						 "who RETAINING hit THAT a THE we TAKEN")
		self.assertEqual(updown.arrange("on I came up were so grandmothers"),
						 "i CAME on WERE up GRANDMOTHERS so")
		self.assertEqual(updown.arrange("way the my wall them him"),
						 "way THE my WALL him THEM")
		self.assertEqual(updown.arrange("turn know great-aunts aunt look A to back"),
						 "turn GREAT-AUNTS know AUNT a LOOK to BACK")

	def test_one_word(self):
		self.assertEqual(updown.arrange("ge"), "ge")

	def test_two_words_correct_order(self):
		self.assertEqual(updown.arrange("ge stop"), "ge STOP")

	def test_set_case(self):
		self.assertEqual(updown.set_case(["ge", "stop", "be", "better"]),
						 ["ge", "STOP", "be", "BETTER"])

	def test_swap_on_even_index(self):
		self.assertEqual(updown.swap_at_index(["ge", "stop", "better", "be"], 2),
						 ["ge", "stop", "be", "better"])

	def test_swap_on_odd_index(self):
		self.assertEqual(updown.swap_at_index(["ge", "be", "stop", "better"], 1),
						 ["ge", "stop", "be", "better"])