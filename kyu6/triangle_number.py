import math

def is_triangle_number(number):
    if type(number) == str:
        return False
    return math.sqrt(8 * number + 1) % 1 == 0
