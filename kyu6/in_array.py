def one_in(word, array):
	return any(word for el in array if word in el)


def in_array(array1, array2):
	return sorted({word for word in array1 if one_in(word, array2)})
