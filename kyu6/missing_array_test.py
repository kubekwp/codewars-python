import unittest
from kyu6 import missing_array


class MissingArrayTest(unittest.TestCase):
	def test_null_array_of_arrays(self):
		self.assert_array(None, 0)

	def test_empty_array_of_arrays(self):
		self.assert_array([], 0)

	def test_null_array(self):
		self.assert_array([None], 0)
		self.assert_array([[1, 2], None], 0)

	def test_empty_array(self):
		self.assert_array([[]], 0)
		self.assert_array([[3, 4], []], 0)

	def test_missing_array(self):
		self.assert_array([[1], [2, 3, 4]], 2)

	def test_lengths(self):
		self.assertEqual(missing_array.lengths([[2, 3, 4, 5], [1, 2]]), [2, 4])

	def test_expected_sum(self):
		self.assertEqual(missing_array.expected_sum([1, 2, 4, 5]), 15)

	def test_examples(self):
		self.assert_array([[1, 2], [4, 5, 1, 1], [1], [5, 6, 7, 8, 9]], 3)
		self.assert_array([[5, 2, 9], [4, 5, 1, 1], [1], [5, 6, 7, 8, 9]], 2)
		self.assert_array([[None], [None, None, None]], 2)
		self.assert_array([['a', 'a', 'a'], ['a', 'a'], ['a', 'a', 'a', 'a'], ['a'], ['a', 'a', 'a', 'a','a', 'a']], 5)

	def assert_array(self, array, missing):
		self.assertEquals(missing_array.get_length_of_missing_array(
			array), missing)
