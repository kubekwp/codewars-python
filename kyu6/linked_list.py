class Node(object):
    def __init__(self, value):
        self.value = value
        self.next = None
        self.prev = None


class DoublyLinkedList(object):
    def __init__(self):
        self.first = None
        self.last = None

    def push(self, elem):
        node = Node(elem)
        if self.first is None:
            self.first = self.last = node
        else:
            node.prev = self.last
            self.last.next = node
            self.last = node

    def pop(self):
        value = self.last.value
        self.last = self.last.prev
        if self.last is None:
            self.first = None
        return value

    def unshift(self, elem):
        node = Node(elem)
        if self.first is None:
            self.first = self.last = node
        else:
            node.next = self.first
            self.first.prev = node
            self.first = node

    def shift(self):
        value = self.first.value
        self.first = self.first.next
        if self.last is None:
            self.first = None
        return value
