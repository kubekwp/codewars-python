def arrange(strng):
	words = strng.split(" ")
	for i in range(len(words) - 1):
		swap_at_index(words, i)
	return " ".join(set_case(words))


def set_case(words):
	return [words[i].lower() if (i % 2 == 0) else words[i].upper() for i in range(len(words))]


def swap_at_index(words, i):
	if ((i % 2 == 0) and (len(words[i]) > len(words[i + 1]))) \
			or ((i % 2 == 1) and (len(words[i]) < len(words[i + 1]))):
		words[i], words[i + 1] = words[i + 1], words[i]
	return words
