#!python3
import unittest
from kyu6.basic_encryption import encrypt


class BasicEncryptionTest(unittest.TestCase):
    def test(self):
        self.assertEquals("", encrypt("", 1))
        self.assertEquals("b", encrypt("a", 1))
        self.assertEquals(chr(2), encrypt(chr(1), 1))
        self.assertEquals("rngcug\"gpet{rv\"og", encrypt("please encrypt me", 2))
        self.assertEquals(chr(1), encrypt(chr(255), 2))
