import unittest
from kyu6.linked_list import DoublyLinkedList

class LinkedListTest(unittest.TestCase):
    def test_push_and_pop(self):
        double_list = DoublyLinkedList()

        double_list.push(10)
        self.assertEqual(10, double_list.pop())

        double_list.push(10)
        double_list.push(20)
        self.assertEqual(20, double_list.pop())
        self.assertEqual(10, double_list.pop())

        double_list.push(10)
        double_list.push(20)
        double_list.push(30)
        self.assertEqual(30, double_list.pop())
        self.assertEqual(20, double_list.pop())
        self.assertEqual(10, double_list.pop())

    def test_shift_and_unshift(self):
        double_list = DoublyLinkedList()

        double_list.unshift(11)
        self.assertEqual(11, double_list.shift())

        double_list.unshift(10)
        double_list.unshift(20)
        self.assertEqual(20, double_list.shift())
        self.assertEqual(10, double_list.shift())

        double_list.unshift(10)
        double_list.unshift(20)
        double_list.unshift(30)
        self.assertEqual(30, double_list.shift())
        self.assertEqual(20, double_list.shift())
        self.assertEqual(10, double_list.shift())

    def test_mix(self):
        double_list = DoublyLinkedList()

        double_list.push(10)
        self.assertEqual(10, double_list.shift())

        double_list.unshift(11)
        self.assertEqual(11, double_list.pop())

        double_list.push(222)
        double_list.unshift(111)
        self.assertEqual(111, double_list.shift())
        self.assertEqual(222, double_list.pop())

        double_list.push(222)
        double_list.unshift(111)
        self.assertEqual(222, double_list.pop())
        self.assertEqual(111, double_list.shift())
