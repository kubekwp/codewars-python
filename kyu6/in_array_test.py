import unittest
from kyu6 import in_array

class SpinWordsTest(unittest.TestCase):

	def test_empty(self):
		self.assertEqual(in_array.in_array([], []), [])

	def test_one_word_not_in_array(self):
		self.assertEqual(in_array.in_array(["word"], ["spider"]), [])

	def test_one_word_exact_in_array(self):
		self.assertEqual(in_array.in_array(["word"], ["word"]), ["word"])

	def test_one_word_substring_in_array(self):
		self.assertEqual(in_array.in_array(["word"], ["buzzword"]), ["word"])
		self.assertEqual(in_array.in_array(["word"], ["spider", "buzzword"]), ["word"])

	def test_multiple_words_not_in_array(self):
		self.assertEqual(in_array.in_array(["word", "duck"],
										   ["buzzwor", "clack", "black"]),
			[])

	def test_multiple_words_in_array(self):
		self.assertEqual(in_array.in_array(["duck", "spider", "word"],
										   ["element", "buzzword", "clack", "biducki"]),
			["duck", "word"])

	def test_multiple_words_in_array_sorted(self):
		self.assertEqual(in_array.in_array(["word", "spider", "duck"],
										   ["element", "buzzword", "clack", "biducki"]),
			["duck", "word"])

	def test_multiple_words_in_array_no_duplicates(self):
		self.assertEqual(in_array.in_array(["word", "duck", "word"],
										   ["element", "buzzword", "clack", "biducki"]),
			["duck", "word"])
