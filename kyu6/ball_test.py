import unittest

from kyu6 import ball


class BallTest(unittest.TestCase):

	def test_zero(self):
		self.assertEquals(ball.max_ball(0), 0)

	def test(self):
		self.assertEquals(ball.max_ball(37), 10)
		self.assertEquals(ball.max_ball(45), 13)
		self.assertEquals(ball.max_ball(99), 28)
		self.assertEquals(ball.max_ball(85), 24)

	def test_to_meter_per_sec(self):
		self.assertEquals(ball.to_meter_per_sec(10), 10 * 1000 / 3600)

	def test_height(self):
		self.assertAlmostEquals(ball.height(10, 0.1), 0.95095, places=5)
		self.assertAlmostEquals(ball.height(10, 0.2), 1.8038, places=5)
		self.assertAlmostEquals(ball.height(10, 0.3), 2.55855, places=5)

