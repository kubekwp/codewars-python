import math

def is_prime(i):
	for j in range(2, int(math.sqrt(i)) + 1):
		if i % j == 0:
			return False
	return True


def step(g, m, n):
	for i in range(m, n - g + 1):
		if is_prime(i) and is_prime(i + g):
			return [i, i + g]
	return None