import unittest

from kyu6 import steps_primes


class StepsPrimesTest(unittest.TestCase):
	def test_no_step(self):
		self.assertEquals(steps_primes.step(2, 2, 4), None)

	def test(self):
		self.assertEquals(steps_primes.step(2, 2, 5), [3, 5])
		self.assertEquals(steps_primes.step(2, 100, 110), [101, 103])
		self.assertEquals(steps_primes.step(4, 100, 110), [103, 107])
		self.assertEquals(steps_primes.step(6, 100, 110), [101, 107])
		self.assertEquals(steps_primes.step(8, 300, 400), [359, 367])
		self.assertEquals(steps_primes.step(10, 300, 400), [307, 317])

	def test_is_prime(self):
		self.assertEquals(steps_primes.is_prime(3), True)
		self.assertEquals(steps_primes.is_prime(17), True)

		self.assertEquals(steps_primes.is_prime(4), False)
		self.assertEquals(steps_primes.is_prime(9), False)

