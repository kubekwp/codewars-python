def street_fighter_selection(fighters, initial_position, moves):
	visited = []
	vpos = initial_position[0]
	hpos = initial_position[1]

	for move in moves:
		if move == "down" and vpos < len(fighters) - 1:
			vpos += 1
		elif move == "up" and vpos > 0:
			vpos -= 1
		elif move == "right":
			hpos = (hpos + 1) % len(fighters[1])
		elif move == "left":
			hpos = (hpos - 1) % len(fighters[1])
		visited.append(fighters[vpos][hpos])

	return visited