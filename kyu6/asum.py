def find_nb(m):
	n = 0
	pile = 0
	while pile < m:
		n += 1
		pile += n * n * n
	return n if pile == m else -1