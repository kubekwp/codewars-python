def get_length_of_missing_array(array_of_arrays):
	if (array_of_arrays is None) or (len(array_of_arrays) == 0):
		return 0

	lgs = lengths(array_of_arrays)
	if 0 in lgs:
		return 0

	return expected_sum(lgs) - sum(lgs)


def lengths(array_of_arrays):
	return sorted([len(array) if array is not None else 0 for array in array_of_arrays])


# def expected_sum(l):
	# return (l[0] + l[len(l) - 1]) * (len(l) + 1) / 2

def expected_sum(l):
	return sum(range(min(l), max(l) + 1))