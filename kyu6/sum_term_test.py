import unittest
from kyu6 import sum_term


class SumTermTest(unittest.TestCase):
    def test(self):
        self.assertEqual(sum_term.sumDig_nthTerm(10, [2, 1, 3], 1), 1)
        self.assertEqual(sum_term.sumDig_nthTerm(10, [2, 1, 3], 2), 1 + 2)
        self.assertEqual(sum_term.sumDig_nthTerm(10, [2, 1, 3], 3), 1 + 3)
        self.assertEqual(sum_term.sumDig_nthTerm(10, [2, 1, 3], 6), 10)
        self.assertEqual(sum_term.sumDig_nthTerm(10, [2, 1, 3], 15), 10)
        self.assertEqual(sum_term.sumDig_nthTerm(10, [2, 1, 3], 50), 9)
        self.assertEqual(sum_term.sumDig_nthTerm(10, [2, 1, 3], 78), 10)
        self.assertEqual(sum_term.sumDig_nthTerm(10, [2, 1, 3], 157), 7)
        self.assertEqual(sum_term.sumDig_nthTerm(10, [2, 2, 5, 8], 6), 11)
        self.assertEqual(sum_term.sumDig_nthTerm(10, [2, 2, 5, 8], 15), 11)
        self.assertEqual(sum_term.sumDig_nthTerm(10, [2, 2, 5, 8], 50), 9)
        self.assertEqual(sum_term.sumDig_nthTerm(10, [2, 2, 5, 8], 78), 11)
        self.assertEqual(sum_term.sumDig_nthTerm(10, [2, 2, 5, 8], 157), 16)
        self.assertEqual(sum_term.sumDig_nthTerm(100, [2, 2, 5, 8], 6), 11)
        self.assertEqual(sum_term.sumDig_nthTerm(100, [2, 2, 5, 8], 15), 11)
        self.assertEqual(sum_term.sumDig_nthTerm(100, [2, 2, 5, 8], 50), 9)
        self.assertEqual(sum_term.sumDig_nthTerm(100, [2, 2, 5, 8], 78), 11)
        self.assertEqual(sum_term.sumDig_nthTerm(100, [2, 2, 5, 8], 157), 16)