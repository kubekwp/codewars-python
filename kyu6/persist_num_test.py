import unittest
from kyu6 import persist_num

class PersistNumTest(unittest.TestCase):

    def test(self):
        self.assertEqual(persist_num.persistence(3), 0)
        self.assertEqual(persist_num.persistence(12), 1)
        self.assertEqual(persist_num.persistence(28), 2)
        self.assertEqual(persist_num.persistence(39), 3)