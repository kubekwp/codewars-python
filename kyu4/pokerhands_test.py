import unittest
from kyu4.pokerhands import PokerHand
from kyu4.pokerhands import Rank
from kyu4 import pokerhands
from random import shuffle
from itertools import chain


class PokerhandsTest(unittest.TestCase):
    def test_detect_rank(self):
        self.assertEqual(Rank.ROYAL_FLUSH,
                         PokerHand("TH JH QH KH AH").calc_rank())
        self.assertEqual(Rank.ROYAL_FLUSH,
                         PokerHand("KS AS TS QS JS").calc_rank())

        self.assertEqual(Rank.STRAIGHT_FLUSH,
                         PokerHand("9D TD JD QD KD").calc_rank())
        self.assertEqual(Rank.STRAIGHT_FLUSH,
                         PokerHand("AC 2C 3C 4C 5C").calc_rank())

        self.assertEqual(Rank.FOUR,
                         PokerHand("AC AS 5C AD AH").calc_rank())
        self.assertEqual(Rank.FOUR,
                         PokerHand("TS TC TH AD TD").calc_rank())

        self.assertEqual(Rank.FULL_HOUSE,
                         PokerHand("TS TC TH AD AD").calc_rank())
        self.assertEqual(Rank.FULL_HOUSE,
                         PokerHand("4S 4C 9H 4D 9D").calc_rank())

        self.assertEqual(Rank.FLUSH,
                         PokerHand("4S 5S 6S 7S 9S").calc_rank())
        self.assertEqual(Rank.FLUSH,
                         PokerHand("2D 4D 6D KD AD").calc_rank())

        self.assertEqual(Rank.STRAIGHT,
                         PokerHand("2H 3D 4C 5D 6S").calc_rank())
        self.assertEqual(Rank.STRAIGHT,
                         PokerHand("2H 3D 4C 5D AS").calc_rank())
        self.assertEqual(Rank.STRAIGHT,
                         PokerHand("TH JD QC KD AS").calc_rank())

        self.assertEqual(Rank.THREE,
                         PokerHand("TH JD TC KD TS").calc_rank())
        self.assertEqual(Rank.THREE,
                         PokerHand("2H 2D TC 2D JS").calc_rank())

        self.assertEqual(Rank.TWO_PAIRS,
                         PokerHand("2H 2D TC TD JS").calc_rank())
        self.assertEqual(Rank.TWO_PAIRS,
                         PokerHand("AH AD TC 7D 7S").calc_rank())

        self.assertEqual(Rank.PAIR,
                         PokerHand("AH AD TC 7D 8S").calc_rank())
        self.assertEqual(Rank.PAIR,
                         PokerHand("2H 3D 3C 7D 8S").calc_rank())

        self.assertEqual(Rank.HIGH_CARD,
                         PokerHand("2H 3D 4C 7D 8S").calc_rank())
        self.assertEqual(Rank.HIGH_CARD,
                         PokerHand("2H 3D AC 9D JS").calc_rank())

    def test_get_card_ranks(self):
        self.assertEqual({14: 1, 13: 1, 12: 1, 11: 1, 10: 1},
                         PokerHand("TH JH QH KH AH").get_card_ranks())
        self.assertEqual({14: 4, 13: 1},
                         PokerHand("AH KH AS AD AC").get_card_ranks())
        self.assertEqual({7: 3, 8: 2},
                         PokerHand("7H 8H 7S 8D 7C").get_card_ranks())

    def test_same_colors_cards(self):
        self.assertTrue(PokerHand("TH JH QH KH AH").same_colors())
        self.assertFalse(PokerHand("TH JH QH KH AC").same_colors())

    def test_card_rank(self):
        self.assertEqual(2, pokerhands.CARD_RANKS["2"])
        self.assertEqual(3, pokerhands.CARD_RANKS["3"])
        self.assertEqual(4, pokerhands.CARD_RANKS["4"])
        self.assertEqual(5, pokerhands.CARD_RANKS["5"])
        self.assertEqual(6, pokerhands.CARD_RANKS["6"])
        self.assertEqual(7, pokerhands.CARD_RANKS["7"])
        self.assertEqual(8, pokerhands.CARD_RANKS["8"])
        self.assertEqual(9, pokerhands.CARD_RANKS["9"])
        self.assertEqual(10, pokerhands.CARD_RANKS["T"])
        self.assertEqual(11, pokerhands.CARD_RANKS["J"])
        self.assertEqual(12, pokerhands.CARD_RANKS["Q"])
        self.assertEqual(13, pokerhands.CARD_RANKS["K"])
        self.assertEqual(14, pokerhands.CARD_RANKS["A"])

    # def test_consecutive_cards(self):
    #     self.assertTrue(PokerHand("TC JS QD KH AH").consecutive())
    #     self.assertTrue(PokerHand("2C 3S 6D 5H 4H").consecutive())
    #     self.assertTrue(PokerHand("AS 2C 3S 5H 4H").consecutive())
    #     self.assertTrue(PokerHand("2C 3S AD 5H 4H").consecutive())
    #     self.assertTrue(PokerHand("9C TS JD QH KH").consecutive())
    #
    #     self.assertFalse(PokerHand("2C 3S 7D 5H 4H").consecutive())
    #     self.assertFalse(PokerHand("2C TS 7D 5H 4H").consecutive())

    def test_compare_hands_various_ranks(self):
        royal = PokerHand("TS JS QS KS AS")
        straight_flush = PokerHand("3C 4C 5C 6C 7C")
        self.assertTrue(royal.__lt__(straight_flush))

        four = PokerHand("3C 5C 5S 5H 5D")
        self.assertTrue(straight_flush.__lt__(four))

        full_house = PokerHand("3C 3C 5S 5H 5D")
        self.assertTrue(four.__lt__(full_house))

        flush = PokerHand("3C 4C TC 6C 9C")
        self.assertTrue(full_house.__lt__(flush))

        straight = PokerHand("3C 4C 5S 6H 7D")
        self.assertTrue(flush.__lt__(straight))

        three = PokerHand("4C 4C 5S 4H 7D")
        self.assertTrue(straight.__lt__(three))

        two_pairs = PokerHand("4C 4C 5S 5H 7D")
        self.assertTrue(three.__lt__(two_pairs))

        pair = PokerHand("4C 4C JS 5H 7D")
        self.assertTrue(two_pairs.__lt__(pair))

        high_card = PokerHand("4C 9C JS 5H AD")
        self.assertTrue(pair.__lt__(high_card))

    def test_compare_hands_straight_flush(self):
        self.assertTrue(PokerHand("JH 9H TH KH QH")
                        .__lt__(PokerHand("2C 3C AC 4C 5C")))
        self.assertTrue(PokerHand("9C TC JC QC KC")
                        .__lt__(PokerHand("8C 9C TC JC QC")))

        self.assertFalse(PokerHand("AC 2C 3C 4C 5C")
                         .__lt__(PokerHand("AS 2S 3S 4S 5S")))
        self.assertFalse(PokerHand("8C 9C TC JC QC")
                         .__lt__(PokerHand("8S 9S TS JS QS")))
        self.assertFalse(PokerHand("8C 9C TC JC QC")
                        .__lt__(PokerHand("9C TC JC QC KC")))
        self.assertFalse(PokerHand("AC 2C 3C 4C 5C")
                        .__lt__(PokerHand("8C 9C TC JC QC")))


    def test_compare_hands_four(self):
        self.assertTrue(PokerHand("TC TH TD TS KC")
                        .__lt__(PokerHand("8C 8H 8D 8S QC")))
        self.assertTrue(PokerHand("TC TH TD TS KC")
                        .__lt__(PokerHand("TC TH TD TS QC")))

    def test_compare_hands_full_house(self):
        self.assertTrue(PokerHand("KH KC 3S 3H 3D")
                         .__lt__(PokerHand("2H 2C 3S 3H 3D")))
        self.assertTrue(PokerHand("8C 8H 8D 9S 9C")
                         .__lt__(PokerHand("2C 2H 2D 5S 5C")))

        self.assertFalse(PokerHand("2C 2H 2D 5S 5C")
                        .__lt__(PokerHand("8C 8H 8D 9S 9C")))

    def test_compare_hands_flush(self):
        self.assertTrue(PokerHand("6S 7S 8S 9S AS")
                         .__lt__(PokerHand("6C 7C 8C 9C KC")))
        self.assertTrue(PokerHand("6S 7S 8S TS AS")
                         .__lt__(PokerHand("6C 7C 8C 9C AC")))
        self.assertTrue(PokerHand("6S 7S 9S TS AS")
                         .__lt__(PokerHand("6C 7C 8C TC AC")))
        self.assertTrue(PokerHand("6S 8S 9S TS AS")
                         .__lt__(PokerHand("6C 7C 9C TC AC")))
        self.assertTrue(PokerHand("7S 8S 9S TS AS")
                         .__lt__(PokerHand("6C 8C 9C TC AC")))

        self.assertFalse(PokerHand("2C 3C 5C 7C 8C")
                        .__lt__(PokerHand("6S 7S 8S 9S AS")))
        self.assertFalse(PokerHand("2C 3C 5C 7C 9C")
                        .__lt__(PokerHand("2S 3S 5S 7S TS")))
        self.assertFalse(PokerHand("6S 7S 8S 9S AS")
                         .__lt__(PokerHand("6D 7D 8D 9D AD")))

    def test_compare_hands_straight(self):
        self.assertTrue(PokerHand("JS QS 9H TS KH")
                        .__lt__(PokerHand("2C 3H AS 4S 5H")))
        self.assertTrue(PokerHand("2S 3H 4H 5S 6C")
                        .__lt__(PokerHand("2D AC 3H 4H 5S")))
        self.assertTrue(PokerHand("6S 7S 8C 9H TD")
                        .__lt__(PokerHand("2C 3C 4S 5H 6D")))
        self.assertTrue(PokerHand("2S 3H 4H 5S 6C")
                        .__lt__(PokerHand("AC 2D 3H 4H 5S")))
        self.assertTrue(PokerHand("TC JC QS KH AD")
                        .__lt__(PokerHand("AS 2C 3C 4S 5H")))

        self.assertFalse(PokerHand("2C 3H AS 4S 5H")
                        .__lt__(PokerHand("JS QS 9H TS KH")))
        self.assertFalse(PokerHand("2D AC 3H 4H 5S")
                        .__lt__(PokerHand("2S 3H 4H 5S 6C")))
        self.assertFalse(PokerHand("2C 3C 4S 5H 6D")
                         .__lt__(PokerHand("6S 7S 8C 9H TD")))
        self.assertFalse(PokerHand("2C 3C 4S 5H 6D")
                         .__lt__(PokerHand("6S 7S 8C 9H TD")))

    def test_compare_three(self):
        self.assertTrue(PokerHand("7S 7C 7D 9H TD")
                        .__lt__(PokerHand("3D 3C 3S 5H 6D")))
        self.assertTrue(PokerHand("7C 7S KH 2H 7H")
                        .__lt__(PokerHand("7C 7S 3S 7H 5S")))
        self.assertTrue(PokerHand("7C 7S KH 5H 7H")
                        .__lt__(PokerHand("7C 7S KS 7H 2S")))

        self.assertFalse(PokerHand("3D 3C 3S 5H 6D")
                         .__lt__(PokerHand("7S 7C 7D 9H TD")))

    def test_compare_two_pairs(self):
        self.assertTrue(PokerHand("2S 2C 9D 9H 2D")
                        .__lt__(PokerHand("3D 3C 5S 5H 6D")))
        self.assertTrue(PokerHand("7S 7C 9D 9H 2D")
                        .__lt__(PokerHand("3D 3C 9C 9S 6D")))
        self.assertTrue(PokerHand("7S 7C 9D 9H TD")
                        .__lt__(PokerHand("7H 7D 9C 9S 6D")))

        self.assertFalse(PokerHand("3D 3C 5S 5H 6D")
                        .__lt__(PokerHand("7S 7C 9D 9H TD")))

    def test_compare_pair(self):
        self.assertTrue(PokerHand("AH AC 5H 6H 7S")
                        .__lt__(PokerHand("AH AC 4H 6H 7S")))

        self.assertTrue(PokerHand("7S 7C 8D 9H TD")
                        .__lt__(PokerHand("3D 3C 5S 6H JD")))
        self.assertTrue(PokerHand("3S 3H 5D 6S KD")
                        .__lt__(PokerHand("3D 3C 5S 6H JD")))

        self.assertFalse(PokerHand("3D 3C 5S 6H JD")
                         .__lt__(PokerHand("7S 7C 8D 9H TD")))

    def test_compare_high_card(self):
        self.assertTrue(PokerHand("3S 4S 8D 9C KD")
                        .__lt__(PokerHand("3D 4C 8S 9H QD")))
        self.assertTrue(PokerHand("3S 4S 8D TC KD")
                        .__lt__(PokerHand("3D 4C 8S 9H KS")))
        self.assertTrue(PokerHand("3S 4S 8D TC KD")
                        .__lt__(PokerHand("3D 4C 6S TH KS")))
        self.assertTrue(PokerHand("3S 5S 6D TC KD")
                        .__lt__(PokerHand("3D 4C 6S TH KS")))
        self.assertTrue(PokerHand("3S 4S 6D TC KD")
                        .__lt__(PokerHand("2D 4C 6S TH KS")))

        self.assertTrue(PokerHand("3S 4S 6D TC KD")
                        .__lt__(PokerHand("2D 4C 6S TH KS")))
        self.assertFalse(PokerHand("3D 3C 5S 6H JD")
                         .__lt__(PokerHand("7S 7C 8D 9H TD")))

    @unittest.skip
    def test_rank_and_progress_of_newly_created_user(self):
        SORTED_POKER_HANDS = list(
            map(PokerHand,
                ["KS AS TS QS JS",
                 "2H 3H 4H 5H 6H",
                 "AS AD AC AH JD",
                 "JS JD JC JH 3D",
                 "2S AH 2H AS AC",
                 "AS 3S 4S 8S 2S",
                 "2H 3H 5H 6H 7H",
                 "2S 3H 4H 5S 6C",
                 "2D AC 3H 4H 5S",
                 "AH AC 5H 6H AS",
                 "2S 2H 4H 5S 4C",
                 "AH AC 5H 6H 7S",
                 "AH AC 4H 6H 7S",
                 "2S AH 4H 5S KC",
                 "2S 3H 6H 7S 9C"]))

        lstCopy = SORTED_POKER_HANDS.copy()
        shuffle(lstCopy)
        userSortedHands = chain(sorted(lstCopy))

        for hand in SORTED_POKER_HANDS:
            self.assertEqual(next(userSortedHands), hand)
