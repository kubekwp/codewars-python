RANK_UPGRADE_THRESHOLD = 100
FIRST_RANK = -8
LAST_RANK = 8

class IncorrectRangeException(Exception):
    pass

class User():
    def __init__(self):
        self.progress = 0
        self.rank = FIRST_RANK

    def inc_progress(self, activity_rank):
        if activity_rank <= -9 or activity_rank == 0 or activity_rank >= 9:
            raise IncorrectRangeException()
        points = calc_progress_points(self.rank, activity_rank)
        self.rank = calculate_rank_after_increment(self.rank, self.progress, points)
        self.progress = calculate_progress(
            self.rank,
            self.progress,
            points)


def calc_progress_points(current_rank, activity_rank):
    rank_difference = calculate_rank_difference(current_rank, activity_rank)
    if rank_difference == 0:
        return 3
    elif rank_difference == -1:
        return 1
    elif current_rank - activity_rank >= 2:
        return 0
    elif rank_difference >= 1:
        return 10 * rank_difference * rank_difference


def calculate_progress(current_rank, current_progress, progress_points):
    return (current_progress + progress_points) % RANK_UPGRADE_THRESHOLD if current_rank < 8 else 0


def calculate_rank_difference(rank_a, rank_b):
    if (rank_a < 0 and rank_b < 0) or (rank_a > 0 and rank_b > 0):
        return rank_b - rank_a
    if rank_a < 0 < rank_b:
        return rank_b - rank_a - 1
    if rank_a > 0 > rank_b:
        return rank_b - rank_a + 1


def calculate_rank_after_increment(rank, progress, progress_points):
    if progress + progress_points >= RANK_UPGRADE_THRESHOLD:
        new_rank = rank + (progress + progress_points) // RANK_UPGRADE_THRESHOLD
    else:
        new_rank = rank
    return min(new_rank, LAST_RANK) if new_rank != 0 else new_rank + 1
