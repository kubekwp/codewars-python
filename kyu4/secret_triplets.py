def recoverSecret(triplets):
	result = ""
	graph = Graph(triplets_to_connections(triplets))
	vertex = graph.get_enter_level_0()
	while vertex:
		result += vertex[0]
		graph.remove(vertex[0])
		vertex = graph.get_enter_level_0()
	return result


def triplets_to_connections(triplets):
	conns = []
	for triplet in triplets:
		conns.append((triplet[0], triplet[1]))
		conns.append((triplet[1], triplet[2]))
	return conns


class Graph(object):
	def __init__(self, connections):
		self.graph = {}
		self.add_connections(connections)

	def add(self, vert1, vert2):
		if not vert1 in self.graph.keys():
			self.graph[vert1] = {vert2}
		else:
			self.graph[vert1].add(vert2)
		if not vert2 in self.graph.keys():
			self.graph[vert2] = set()

	def add_connections(self, connections):
		for vert1, vert2 in connections:
			self.add(vert1, vert2)

	def remove(self, vert):
		for v in self.graph.keys():
			if vert in self.graph[v]:
				self.graph[v].remove(vert)
		del self.graph[vert]

	def get_enter_level_0(self):
		result = []
		for vert in self.graph.keys():
			if len(self.graph.keys()) == 1 or not self.has_incoming_edges(vert):
				result.append(vert)
		return result

	def has_incoming_edges(self, vert):
		has_incoming = False
		for in_vert in self.graph.keys():
			has_incoming = has_incoming or vert in self.graph[in_vert]
		return has_incoming

	def vertices(self):
		return sorted([vert for vert in self.graph.keys()])
