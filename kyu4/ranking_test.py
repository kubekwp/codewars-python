import unittest
from kyu4 import ranking


class RankingTest(unittest.TestCase):
    def setUp(self):
        self.user = ranking.User();

    def test_rank_and_progress_of_newly_created_user(self):
        self.assertEqual(-8, self.user.rank)
        self.assertEqual(0, self.user.progress)

    def test_multiple_increments_cumulate_progress(self):
        self.user.inc_progress(self.user.rank)
        self.assertEqual(3, self.user.progress)

        self.user.inc_progress(self.user.rank)
        self.assertEqual(6, self.user.progress)

        self.user.inc_progress(self.user.rank)
        self.assertEqual(9, self.user.progress)

    def test_calculate_progress_points(self):
        # same rank
        self.assertEqual(3, ranking.calc_progress_points(-8, -8),
                         "Same rank activity should result in 3 points")

        # one rank lower
        self.assertEqual(1, ranking.calc_progress_points(-5, -6),
                         "One rank lower activity should result in 1 point")
        self.assertEqual(1, ranking.calc_progress_points(-4, -5))
        self.assertEqual(1, ranking.calc_progress_points(4, 3))
        self.assertEqual(1, ranking.calc_progress_points(8, 7))
        self.assertEqual(1, ranking.calc_progress_points(1, -1))

        # two or more ranks lower
        self.assertEqual(0, ranking.calc_progress_points(-4, -6),
                         "Two or more ranks lower activity should result in 0 points")
        self.assertEqual(0, ranking.calc_progress_points(4, 2))
        self.assertEqual(0, ranking.calc_progress_points(4, 1))

        # higher rakns
        self.assertEqual(10, ranking.calc_progress_points(1, 2))
        self.assertEqual(40, ranking.calc_progress_points(1, 3))
        self.assertEqual(90, ranking.calc_progress_points(1, 4))
        self.assertEqual(160, ranking.calc_progress_points(1, 5))

    def test_calculate_rank_difference(self):
        self.assertEqual(0, ranking.calculate_rank_difference(-8, -8))
        self.assertEqual(0, ranking.calculate_rank_difference(-1, -1))
        self.assertEqual(0, ranking.calculate_rank_difference(1, 1))
        self.assertEqual(0, ranking.calculate_rank_difference(7, 7))

        self.assertEqual(1, ranking.calculate_rank_difference(-8, -7))
        self.assertEqual(1, ranking.calculate_rank_difference(-2, -1))
        self.assertEqual(1, ranking.calculate_rank_difference(1, 2))
        self.assertEqual(1, ranking.calculate_rank_difference(5, 6))

        self.assertEqual(4, ranking.calculate_rank_difference(-8, -4))
        self.assertEqual(4, ranking.calculate_rank_difference(2, 6))

        self.assertEqual(-1, ranking.calculate_rank_difference(-7, -8))
        self.assertEqual(-1, ranking.calculate_rank_difference(-1, -2))
        self.assertEqual(-1, ranking.calculate_rank_difference(7, 6))

        self.assertEqual(-4, ranking.calculate_rank_difference(7, 3))
        self.assertEqual(-4, ranking.calculate_rank_difference(-2, -6))

        # rank 0 has to be excluded
        self.assertEqual(4, ranking.calculate_rank_difference(-2, 3),
                         "0 rank should be skipped")
        self.assertEqual(1, ranking.calculate_rank_difference(-1, 1))
        self.assertEqual(-1, ranking.calculate_rank_difference(1, -1))
        self.assertEqual(-4, ranking.calculate_rank_difference(2, -3))

    def test_calculate_rank_after_increment(self):
        self.assertEqual(-8, ranking.calculate_rank_after_increment(-8, 0, 10))
        self.assertEqual(-8, ranking.calculate_rank_after_increment(-8, 89, 10))

        self.assertEqual(5, ranking.calculate_rank_after_increment(5, 0, 10))
        self.assertEqual(5, ranking.calculate_rank_after_increment(5, 89, 10))

        # rank upgrade threshold exceeded
        self.assertEqual(-7, ranking.calculate_rank_after_increment(-8, 90, 10),
                         "After reaching 100 progress, rank should increment")
        self.assertEqual(-7, ranking.calculate_rank_after_increment(-8, 99, 2))
        self.assertEqual(6, ranking.calculate_rank_after_increment(5, 90, 10))
        self.assertEqual(6, ranking.calculate_rank_after_increment(5, 99, 2))
        self.assertEqual(1, ranking.calculate_rank_after_increment(-1, 90, 10))
        self.assertEqual(1, ranking.calculate_rank_after_increment(-1, 99, 2))

        # more than one rank upgrade threshold exceeded
        self.assertEqual(-6, ranking.calculate_rank_after_increment(-8, 0, 200))
        self.assertEqual(6, ranking.calculate_rank_after_increment(4, 90, 120))
        self.assertEqual(6, ranking.calculate_rank_after_increment(4, 90, 120))

        # rank 8 reached
        self.assertEqual(8, ranking.calculate_rank_after_increment(8, 0, 110),
                         "No progress increment when 8 rank was reached.")
        self.assertEqual(8, ranking.calculate_rank_after_increment(7, 0, 900))

    def test_calculate_progress_after_increment(self):
        self.assertEqual(10, ranking.calculate_progress(-8, 0, 10))
        self.assertEqual(0, ranking.calculate_progress(1, 0, 100))
        self.assertEqual(1, ranking.calculate_progress(-1, 100, 1))

        # rank upgrade threshold exceeded
        self.assertEqual(1, ranking.calculate_progress(-5, 0, 101))
        self.assertEqual(20, ranking.calculate_progress(5, 0, 120))
        self.assertEqual(1, ranking.calculate_progress(-1, 99, 2))
        self.assertEqual(10, ranking.calculate_progress(1, 90, 20))

        # no progress after reaching rank 8
        self.assertEqual(0, ranking.calculate_progress(8, 0, 20))

    def test_error_on_incorrect_rank(self):
        for incorrect_rank in [-9, 0, 9]:
            with self.assertRaises(ranking.IncorrectRangeException):
                self.user.inc_progress(incorrect_rank)

    def test_scenario(self):
        self.assertEqual(-8, self.user.rank)
        self.assertEqual(0, self.user.progress)

        self.user.inc_progress(-7)
        self.assertEqual(-8, self.user.rank)
        self.assertEqual(10, self.user.progress)

        self.user.inc_progress(-5)
        self.assertEqual(-7, self.user.rank)
        self.assertEqual(0, self.user.progress)

        self.user.inc_progress(-1)
        self.assertEqual(-4, self.user.rank)
        self.assertEqual(60, self.user.progress)

        self.user.inc_progress(4)
        self.assertEqual(1, self.user.rank)
        self.assertEqual(50, self.user.progress)

        self.user.inc_progress(8)
        self.assertEqual(6, self.user.rank)
        self.assertEqual(40, self.user.progress)

        self.user.inc_progress(8)
        self.assertEqual(6, self.user.rank)
        self.assertEqual(80, self.user.progress)

        self.user.inc_progress(8)
        self.assertEqual(7, self.user.rank)
        self.assertEqual(20, self.user.progress)
