def lcs(x, y):
    if (len(x) == 0) or (len(y) == 0):
        return ""
    elif x[-1] == y[-1]:
        return lcs(x[:-1], y[:-1]) + x[-1]
    else:
        return max_lcs(lcs(x, y[:-1]), lcs(x[:-1], y))


def max_lcs(x, y):
    return max(x, y, key=len)
