import unittest
from kyu4 import lcs

class LCSTest(unittest.TestCase):

    def test_lcs_empty_when_one_of_strings_empty(self):
        self.assertEqual("", lcs.lcs("abcd", ""))
        self.assertEqual("", lcs.lcs("", "abcd"))

    def test_lcs_one_letter(self):
        self.assertEqual("a", lcs.lcs("a", "a"))
        self.assertEqual("", lcs.lcs("b", "c"))

    def test_lcs_two_letters(self):
        self.assertEqual("ab", lcs.lcs("ab", "ab"))
        self.assertEqual("a", lcs.lcs("ab", "ac"))

    def test_lcs_more_letters(self):
        self.assertEqual("abc", lcs.lcs("abcdef", "abc"))
        self.assertEqual("acf", lcs.lcs("abcdef", "acf"))

    def test_max(self):
        self.assertEqual("abc", lcs.max_lcs("abc", "zy"))
        self.assertEqual("bbbb", lcs.max_lcs("aa", "bbbb"))