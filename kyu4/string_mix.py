def mix(s1, s2):
    return construct_mix(
        sort_substrings_by_length_then_lexicographic(
            filter_empty_substrings(
                get_substrings_for_all_letters(s1, s2))))


def filter_empty_substrings(substrings):
    return [sub for sub in substrings if sub]


def get_substrings_for_all_letters(s1, s2):
    return [get_max_substring(create_count_substring(chr(letter_ord), s1), create_count_substring(chr(letter_ord), s2))
            for letter_ord in range(ord('a'), ord('z') + 1)]


def get_max_substring(s1, s2):
    return get_longer_or_equal_substring(s1, s2) if (s1 and s2) else get_from_existing(s1, s2)


def create_count_substring(letter, s):
    count = s.count(letter)
    return letter * count if count > 1 else None


def construct_mix(substrings):
    return "/".join(substrings)


def get_from_existing(s1, s2):
    if s1:
        return "1:" + s1
    elif s2:
        return "2:" + s2


def get_longer_or_equal_substring(s1, s2):
    if len(s1) == len(s2):
        return "=:" + s1
    else:
        return "1:" + s1 if len(s1) > len(s2) else "2:" + s2


def sort_substrings_by_length_then_lexicographic(subs):
    sorted_lexicographic = sorted(subs)
    return sorted(sorted_lexicographic, key=len, reverse=True)
