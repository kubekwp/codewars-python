#!python3
from enum import IntEnum, unique


@unique
class Rank(IntEnum):
    ROYAL_FLUSH = 1
    STRAIGHT_FLUSH = 2
    FOUR = 3
    FULL_HOUSE = 4
    FLUSH = 5
    STRAIGHT = 6
    THREE = 7
    TWO_PAIRS = 8
    PAIR = 9
    HIGH_CARD = 10


def compare_four(ranks1, ranks2):
    four_key_1 = find_key_for_num(ranks1, 4)
    four_key_2 = find_key_for_num(ranks2, 4)
    if four_key_1 != four_key_2:
        return four_key_1 > four_key_2
    else:
        return find_key_for_num(ranks1, 1) > find_key_for_num(ranks2, 1)


def compare_full_house(ranks1, ranks2):
    three_key_1 = find_key_for_num(ranks1, 3)
    three_key_2 = find_key_for_num(ranks2, 3)
    if three_key_1 != three_key_2:
        return three_key_1 > three_key_2
    else:
        return find_key_for_num(ranks1, 2) > find_key_for_num(ranks2, 2)


def compare_flush(ranks1, ranks2):
    if len(ranks1.keys() - ranks2.keys()) > 0:
        return max(ranks1.keys() - ranks2.keys()) > max(ranks2.keys() - ranks1.keys())
    return False


def compare_straight(ranks1, ranks2):
    if CARD_RANKS["A"] in ranks2:
        if CARD_RANKS["A"] not in ranks1:
            return min(ranks2) == 2
        else:
            return min(ranks1) > min(ranks2)

    if CARD_RANKS["A"] in ranks1:
        return min(ranks1) != 2
    else:
        return min(ranks1) > min(ranks2)


def compare_three(ranks1, ranks2):
    three_key_1 = find_key_for_num(ranks1, 3)
    three_key_2 = find_key_for_num(ranks2, 3)
    if three_key_1 != three_key_2:
        return three_key_1 > three_key_2
    return max(ranks1.keys() - ranks2.keys()) > max(ranks2.keys() - ranks1.keys())


def compare_two_pairs(ranks1, ranks2):
    pairs1 = [key for key in ranks1 if ranks1[key] == 2]
    pairs2 = [key for key in ranks2 if ranks2[key] == 2]
    if max(pairs1) != max(pairs2):
        return max(pairs1) > max(pairs2)
    else:
        if min(pairs1) != min(pairs2):
            return min(pairs1) > min(pairs2)
        else:
            return max(ranks1.keys() - pairs1) > max(ranks2.keys() - pairs2)


def compare_pair(ranks1, ranks2):
    pair_card_1 = find_key_for_num(ranks1, 2)
    pair_card_2 = find_key_for_num(ranks2, 2)

    if pair_card_1 != pair_card_2:
        return pair_card_1 > pair_card_2
    else:
        return max(ranks1.keys() - ranks2.keys()) > max(ranks2.keys() - ranks1.keys())


def compare_high(ranks1, ranks2):
    return max(ranks1.keys() - ranks2.keys()) > max(ranks2.keys() - ranks1.keys())


def find_key_for_num(ranks, num):
    for key in ranks.keys():
        if ranks[key] == num:
            return key


COMPARATORS = {Rank.STRAIGHT_FLUSH: compare_straight,
               Rank.FOUR: compare_four,
               Rank.FULL_HOUSE: compare_full_house,
               Rank.FLUSH: compare_flush,
               Rank.STRAIGHT: compare_straight,
               Rank.THREE: compare_three,
               Rank.TWO_PAIRS: compare_two_pairs,
               Rank.PAIR: compare_pair,
               Rank.HIGH_CARD: compare_high}


class PokerHand(object):
    def __repr__(self):
        return self.hand

    def __init__(self, hand):
        self.hand = hand
        self.rank = self.calc_rank()

    def __lt__(self, other):
        this_rank = self.rank
        other_rank = other.rank
        if this_rank is not other_rank:
            return this_rank < other_rank
        return COMPARATORS[this_rank](self.get_card_ranks(), other.get_card_ranks())

    def calc_rank(self):
        card_ranks = self.get_card_ranks()
        if self.same_colors() and self.consecutive(card_ranks):
            if CARD_RANKS["T"] == min(card_ranks):
                return Rank.ROYAL_FLUSH
            else:
                return Rank.STRAIGHT_FLUSH
        if self.is_four(card_ranks):
            return Rank.FOUR
        if self.is_full_house(card_ranks):
            return Rank.FULL_HOUSE
        if self.same_colors():
            return Rank.FLUSH
        if self.consecutive(card_ranks):
            return Rank.STRAIGHT
        if self.is_three(card_ranks):
            return Rank.THREE
        if self.is_two_pairs(card_ranks):
            return Rank.TWO_PAIRS
        if self.is_pair(card_ranks):
            return Rank.PAIR

        return Rank.HIGH_CARD

    def same_colors(self):
        colors = set()
        for card in self.hand.split(" "):
            colors.add(card[1])
        return len(colors) == 1

    def consecutive(self, card_ranks):
        different_ranks = len(card_ranks) == 5
        if CARD_RANKS["A"] in card_ranks:
            return different_ranks \
                   and (sum(card_ranks) == 28 or min(card_ranks) == CARD_RANKS["T"])
        else:
            return different_ranks \
                   and (max(card_ranks) - min(card_ranks) == 4)

    def is_four(self, card_ranks):
        return 4 in card_ranks.values()

    def is_full_house(self, card_ranks):
        return (len(card_ranks) == 2) and (3 in card_ranks.values() or 2 in card_ranks.values())

    def is_three(self, card_ranks):
        return (len(card_ranks) == 3) and 3 in card_ranks.values() and 1 in card_ranks.values()

    def is_two_pairs(self, card_ranks):
        return (len(card_ranks) == 3) and 2 in card_ranks.values() and 1 in card_ranks.values()

    def is_pair(self, card_ranks):
        return len(card_ranks) == 4

    def get_card_ranks(self):
        card_ranks = {}
        for card in self.hand.split(" "):
            rank = CARD_RANKS[card[0]]
            if rank in card_ranks:
                card_ranks[rank] += 1
            else:
                card_ranks[rank] = 1
        return card_ranks


CARD_RANKS = {"1": 1, "2": 2, "3": 3,
              "4": 4, "5": 5, "6": 6,
              "7": 7, "8": 8, "9": 9,
              "T": 10, "J": 11, "Q": 12,
              "K": 13, "A": 14}
