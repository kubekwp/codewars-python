import unittest
from kyu4 import string_mix


class StringsMixTest(unittest.TestCase):
    def test_empty(self):
        self.assertEqual("", string_mix.mix("", ""))

    def test_no_letter_strings(self):
        self.assertEqual("", string_mix.mix("!@#$%^&*()-=_+", "!@#$%^&*()-=_+"))

    def test_one_letter_string(self):
        for letter in range(ord('a'), ord('z') + 1):
            self.assertEqual("", string_mix.mix(chr(letter), chr(letter)))

    def test_two_letters_in_string_1(self):
        self.assertEqual("1:aa", string_mix.mix("aa", ""))
        self.assertEqual("1:aa", string_mix.mix("aba", ""))
        self.assertEqual("1:aa", string_mix.mix("a  a", ""))

        self.assertEqual("1:tt", string_mix.mix("tt", ""))
        self.assertEqual("1:tt", string_mix.mix("tat", ""))
        self.assertEqual("1:tt", string_mix.mix("t t", ""))

    def test_two_letters_in_string_2(self):
        self.assertEqual("2:bb", string_mix.mix("", "bb"))
        self.assertEqual("2:bb", string_mix.mix("", "bab"))
        self.assertEqual("2:bb", string_mix.mix("", "b  b"))

    def test_multiple_letters_in_string_1(self):
        self.assertEqual("1:ccc", string_mix.mix("c1c c", "vc"))
        self.assertEqual("1:cccc", string_mix.mix("cacecic", "vc"))

    def test_multiple_letters_in_string_2(self):
        self.assertEqual("2:ddd", string_mix.mix("vd", "d1d d"))
        self.assertEqual("2:dddd", string_mix.mix("vc", "dadedid"))

    def test_get_not_empty_substrings(self):
        self.assertEqual(["1:cc", "1:ddd"],
                         string_mix.filter_empty_substrings(
                             string_mix.get_substrings_for_all_letters("dd cvc d", "dvc")))

    def test_multiple_different_letters_in_string_1(self):
        self.assertEqual("1:ccc/1:aa", string_mix.mix("a cac c", "vc"))

    def test_multiple_different_letters_in_string_2(self):
        self.assertEqual("2:ccc/2:aa", string_mix.mix("vc", "a cac c"))

    def test_same_letter_max_in_1(self):
        self.assertEqual("1:ccc/1:aa", string_mix.mix("a cac c", "cc a"))

    def test_same_letter_max_in_2(self):
        self.assertEqual("2:ccc/2:aa", string_mix.mix("cc a", "a cac c"))

    def test_same_letter_same_count(self):
        self.assertEqual("=:ccc/=:aa", string_mix.mix("a cac c@@", "a cac c$$"))

    def test_get_max_substring(self):
        self.assertEqual(None, string_mix.get_max_substring(None, None))
        self.assertEqual("1:aaa", string_mix.get_max_substring("aaa", None))
        self.assertEqual("2:aaa", string_mix.get_max_substring(None, "aaa"))
        self.assertEqual("1:aaa", string_mix.get_max_substring("aaa", "aa"))
        self.assertEqual("2:aaa", string_mix.get_max_substring("aa", "aaa"))
        self.assertEqual("=:aaa", string_mix.get_max_substring("aaa", "aaa"))

    def test_sort_by_length_than_lexicographic(self):
        self.assertEqual(["2:dddd", "1:ccc"],
                         string_mix.sort_substrings_by_length_then_lexicographic(["2:dddd", "1:ccc"]))
        self.assertEqual(["2:dddd", "1:ccc"],
                         string_mix.sort_substrings_by_length_then_lexicographic(["1:ccc", "2:dddd"]))
        self.assertEqual(["1:dddd", "1:ccc"],
                         string_mix.sort_substrings_by_length_then_lexicographic(["1:ccc", "1:dddd"]))
        self.assertEqual(["2:yy", "=:hh"],
                         string_mix.sort_substrings_by_length_then_lexicographic(["=:hh", "2:yy"]))
        self.assertEqual(["=:wwww", "1:ccc", "1:ddd"],
                         string_mix.sort_substrings_by_length_then_lexicographic(["1:ddd", "1:ccc", "=:wwww"]))

    def test_samples(self):
        self.assertEqual(string_mix.mix("Are they here", "yes, they are here"),
                         "2:eeeee/2:yy/=:hh/=:rr")
        self.assertEqual(string_mix.mix("looping is fun but dangerous", "less dangerous than coding"),
                         "1:ooo/1:uuu/2:sss/=:nnn/1:ii/2:aa/2:dd/2:ee/=:gg")
        self.assertEqual(string_mix.mix(" In many languages", " there's a pair of functions"),
                         "1:aaa/1:nnn/1:gg/2:ee/2:ff/2:ii/2:oo/2:rr/2:ss/2:tt")
        self.assertEqual(string_mix.mix("Lords of the Fallen", "gamekult"),
                         "1:ee/1:ll/1:oo")
        self.assertEqual(string_mix.mix("codewars", "codewars"),
                         "")
        self.assertEqual(string_mix.mix("A generation must confront the looming ", "codewarrs"),
                         "1:nnnnn/1:ooooo/1:tttt/1:eee/1:gg/1:ii/1:mm/=:rr")
