import unittest
from kyu4 import secret_triplets


class SecretTripletsTest(unittest.TestCase):
	def test_recover_secret(self):
		self.assertEqual("sto", secret_triplets.recoverSecret([["s", "t", "o"]]))
		self.assertEqual("whatisup",
						 secret_triplets.recoverSecret([
							 ['t', 'u', 'p'],
							 ['w', 'h', 'i'],
							 ['t', 's', 'u'],
							 ['a', 't', 's'],
							 ['h', 'a', 'p'],
							 ['t', 'i', 's'],
							 ['w', 'h', 's']
						 ]
						 ))


	def test(self):
		self.assertEqual([("s", "t"), ("t", "o")],
						 secret_triplets.triplets_to_connections([["s", "t", "o"]]))

		graph = secret_triplets.Graph(secret_triplets.triplets_to_connections([["s", "t", "o"]]))
		self.assertEqual(["o", "s", "t"], graph.vertices())
		self.assertEqual(["s"], graph.get_enter_level_0())
		self.assertEqual("s", graph.get_enter_level_0()[0])

		graph.remove(graph.get_enter_level_0()[0])

		self.assertEqual(["t"], graph.get_enter_level_0())
		graph.remove("t")
		self.assertEqual(["o"], graph.get_enter_level_0())


	def test_graph_add(self):
		graph = secret_triplets.Graph(())
		graph.add("a", "b")

		self.assertEqual(["a", "b"], graph.vertices())
		self.assertTrue(graph.is_connected("a", "b"))

		graph.add("f", "c")

		self.assertSequenceEqual(["a", "b", "c", "f"], graph.vertices())
		self.assertFalse(graph.is_connected("a", "c"))
		self.assertTrue(graph.is_connected("f", "c"))

		graph.add("f", "d")

		self.assertSequenceEqual(["a", "b", "c", "d", "f"], graph.vertices())
		self.assertTrue(graph.is_connected("f", "d"))


	def test_graph_init_from_connections(self):
		graph = secret_triplets.Graph([("a", "b"), ("f", "c"), ("f", "d")]);

		self.assertTrue(graph.is_connected("a", "b"))
		self.assertFalse(graph.is_connected("a", "c"))
		self.assertTrue(graph.is_connected("f", "c"))
		self.assertTrue(graph.is_connected("f", "d"))


	def test_graph_remove(self):
		graph = secret_triplets.Graph([("a", "b"), ("a", "c"), ("a", "d"), ("b", "c"), ("d", "c")]);

		self.assertTrue(graph.is_connected("a", "b"))
		self.assertTrue(graph.is_connected("b", "c"))

		graph.remove("b")

		self.assertFalse(graph.is_connected("a", "b"))
		self.assertFalse(graph.is_connected("b", "c"))


	def test_graph_get_enter_level_0(self):
		graph = secret_triplets.Graph([]);
		self.assertEqual([], graph.get_enter_level_0())

		graph = secret_triplets.Graph([("a", "b")])
		self.assertEqual(["a"], graph.get_enter_level_0())
		graph.remove("a")
		self.assertEqual(["b"], graph.get_enter_level_0())

		graph = secret_triplets.Graph([("a", "b"), ("a", "c"), ("a", "d"), ("b", "c"), ("d", "c")]);
		self.assertEqual(["a"], graph.get_enter_level_0())

		graph = secret_triplets.Graph([("e", "b"), ("a", "b"), ("a", "c"), ("a", "d"), ("b", "c"), ("d", "c")]);
		self.assertTrue("a" in graph.get_enter_level_0())
		self.assertTrue("e" in graph.get_enter_level_0())


	def test_triplets_to_connections(self):
		self.assertEqual([("w", "h"), ("h", "i")],
						 secret_triplets.triplets_to_connections([['w', 'h', 'i']]))

		self.assertEqual([("w", "h"), ("h", "i"), ("t", "s"), ("s", "i")],
						 secret_triplets.triplets_to_connections([['w', 'h', 'i'],
																  ['t', 's', 'i']]))


