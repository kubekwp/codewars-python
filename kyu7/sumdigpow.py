def num_to_digs_int(a):
	if int(a / 10) > 0:
		return [a % 10] + num_to_digs_int(int(a / 10))
	return [a % 10]


def num_to_digs(a):
	return list(reversed(num_to_digs_int(a)))


def sum_pow(digs):
	p = 1
	sum = 0
	for dig in digs:
		sum += pow(dig, p)
		p += 1
	return sum


def sum_dig_pow(a, b):
	return [x for x in range(a, b + 1) if (sum_pow(num_to_digs(x)) == x)]