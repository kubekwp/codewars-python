import unittest

from kyu7 import feuer_frei


class FeuerFreiTest(unittest.TestCase):
	def test_empty(self):
		self.assertEqual("90 Stunden mehr Benzin ben%stigt." % chr(246),
		                 feuer_frei.feuer_frei(5, 2))

if __name__ == "__main__":
	unittest.main()