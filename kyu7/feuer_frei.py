def feuer_frei(concentration, barrels):
    exceed = concentration * barrels - 100
    if exceed == 0:
        return "Perfekt!"
    if exceed > 0:
        return exceed
    else:
        return (str(-exceed) + " Stunden mehr Benzin ben%stigt.") % chr(246)
