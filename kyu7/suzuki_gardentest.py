import unittest
from kyu7 import suzuki_garden

class SuzukiGardenTest(unittest.TestCase):
	def test_single_gravel_or_rock(self):
		self.assertEqual("gravel", suzuki_garden.rake_garden("gravel"))
		self.assertEqual("rock", suzuki_garden.rake_garden("rock"))

	def test_single_other_word(self):
		self.assertEqual("gravel", suzuki_garden.rake_garden("spider"))

	def test_multiple_gravel_or_rock(self):
		self.assertEqual("gravel rock rock gravel", suzuki_garden.rake_garden("gravel rock rock gravel"))

	def test_multiple_other(self):
		self.assertEqual("gravel gravel gravel rock gravel", suzuki_garden.rake_garden("spider gravel dupa rock dupa"))
