def solution0(number):
	sum = 0

	for e in range(1, number):
		if (e % 3 == 0) or (e % 5 == 0):
			sum += e

	return sum


def mod35(x):
	return (x % 3 == 0) or (x % 5 == 0)


def solution(number):
	sum = 0

	for e in [x for x in range(1, number) if mod35(x)]:
		sum += e

	return sum

print(solution(10))