import unittest

from kyu7 import order


class OrderTest(unittest.TestCase):
	def test_empty(self):
		self.assertEqual("", order.order(""))

	def test_one_word(self):
		self.assertEqual("wor7d", order.order("wor7d"))

	def test_more_words(self):
		self.assertEqual(
			"another1 3other wor7d", order.order("wor7d 3other another1"))

	def test_to_num(self):
		self.assertEqual(1, order.to_num("1aord"))
		self.assertEqual(9, order.to_num("ao9rd"))
		self.assertEqual(7, order.to_num("aord7"))

if __name__ == "__main__":
	unittest.main()