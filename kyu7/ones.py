# def binary_array_to_number(arr):
# 	dec = 0
# 	pow = len(arr) - 1
# 	for el in arr:
# 		dec += el * 2 ** pow
# 		pow -= 1
# 	return dec

def binary_array_to_number(arr):
	size = len(arr)
	return sum([(arr[size - pow - 1] * (2 ** pow)) for pow in range(size)])