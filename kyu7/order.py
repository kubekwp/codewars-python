def to_num(word):
	for ch in word:
		if ord(ch) >= 49 and ord(ch) <= 58:
			return ord(ch) - 48


def order(sentence):
	return " ".join(sorted(sentence.split(), key=to_num))