import unittest

from kyu7 import ones


class OnesTest(unittest.TestCase):
	def test_empty(self):
		self.assertEqual(ones.binary_array_to_number([0, 0, 0, 1]), 1)
		self.assertEqual(ones.binary_array_to_number([1, 0]), 2)
		self.assertEqual(ones.binary_array_to_number([1, 0, 0]), 4)
		self.assertEqual(ones.binary_array_to_number([1, 0, 0, 0]), 8)
		self.assertEqual(ones.binary_array_to_number([0, 0, 1, 1]), 3)
		self.assertEqual(ones.binary_array_to_number([1, 1, 1, 1]), 15)
		self.assertEqual(ones.binary_array_to_number([0, 1, 1, 0]), 6)


if __name__ == "__main__":
	unittest.main()