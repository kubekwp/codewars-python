import unittest

from kyu7 import accum


class AccumTest(unittest.TestCase):
	def test_empty(self):
		self.assertEqual("", accum.accum(""))

	def test_one_letter(self):
		self.assertEqual("A", accum.accum("a"))
		self.assertEqual("F", accum.accum("f"))
		self.assertEqual("K", accum.accum("K"))

	def test_two_letters(self):
		self.assertEqual("A-Bb", accum.accum("ab"))

	def test_more_letters(self):
		self.assertEqual("A-Bb-Ccc-Ffff", accum.accum("abcf"))