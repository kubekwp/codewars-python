def accum(s):
	idx = 0
	acc = ""
	for l in s:
		acc += l.upper()
		for i in range(idx):
			acc += l.lower()
		acc += "-"
		idx += 1
	return acc[:-1]