import unittest

from kyu7 import sumdigpow


class SumDigPowTest(unittest.TestCase):

	def test_sum_dig_pow(self):
		self.assertEqual(sumdigpow.sum_pow([1]), 1)
		self.assertEqual(sumdigpow.sum_pow([9]), 9)
		self.assertEqual(sumdigpow.sum_pow([1, 2]), 1 + pow(2, 2))
		self.assertEqual(sumdigpow.sum_pow([1, 3, 5]), 1 + pow(3, 2) + pow(5, 3))

		self.assertEqual(sumdigpow.num_to_digs(2), [2])
		self.assertEqual(sumdigpow.num_to_digs(19), [1, 9])
		self.assertEqual(sumdigpow.num_to_digs(135), [1, 3, 5])
		self.assertEqual(sumdigpow.num_to_digs(1357), [1, 3, 5, 7])

		self.assertEqual(sumdigpow.sum_dig_pow(1, 1), [1])
		self.assertEqual(sumdigpow.sum_dig_pow(2, 2), [2])
		self.assertEqual(sumdigpow.sum_dig_pow(9, 9), [9])
		self.assertEqual(sumdigpow.sum_dig_pow(10, 10), [])
		self.assertEqual(sumdigpow.sum_dig_pow(23, 23), [])
		self.assertEqual(sumdigpow.sum_dig_pow(89, 89), [89])
		self.assertEqual(sumdigpow.sum_dig_pow(1, 2), [1, 2])
		self.assertEqual(sumdigpow.sum_dig_pow(1, 10), [1, 2, 3, 4, 5, 6, 7, 8, 9])
		self.assertEqual(sumdigpow.sum_dig_pow(1, 100), [1, 2, 3, 4, 5, 6, 7, 8, 9, 89])
		self.assertEqual(sumdigpow.sum_dig_pow(10, 89),  [89])
		self.assertEqual(sumdigpow.sum_dig_pow(10, 100),  [89])
		self.assertEqual(sumdigpow.sum_dig_pow(90, 100), [])
		self.assertEqual(sumdigpow.sum_dig_pow(89, 135), [89, 135])
		pass

if __name__ == '__main__':
    unittest.main()
