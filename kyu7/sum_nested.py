def sum_nested(lst):
	return sum([el if isinstance(el, int) else sum_nested(el) for el in lst ])