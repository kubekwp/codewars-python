import unittest

from kyu7 import getsum


class GetSumTest(unittest.TestCase):
	def test_same_numbers(self):
		self.assertEqual(1, getsum.get_sum(1, 1))
		self.assertEqual(-1, getsum.get_sum(-1, -1))

	def test_sum_two_consecutive(self):
		self.assertEqual(3, getsum.get_sum(1, 2))
		self.assertEqual(-5, getsum.get_sum(-3, -2))

	def test_sum_two_reversed(self):
		self.assertEqual(3, getsum.get_sum(2, 1))
		self.assertEqual(-5, getsum.get_sum(-2, -3))

	def test_sum_more_than_two_between(self):
		self.assertEqual(10, getsum.get_sum(1, 4))
		self.assertEqual(-9, getsum.get_sum(-4, -2))

	def test_sum_more_than_two_between_reversed(self):
		self.assertEqual(10, getsum.get_sum(4, 1))
		self.assertEqual(-9, getsum.get_sum(-2, -4))

if __name__ == "__main__":
	unittest.main()