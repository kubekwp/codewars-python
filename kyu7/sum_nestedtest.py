import unittest
from kyu7.sum_nested import sum_nested


class SumNestedTest(unittest.TestCase):
    def test_empty(self):
        self.assertEqual(0, sum_nested([]))
        self.assertEqual(0, sum_nested([[], []]))

    def test_not_nested(self):
        self.assertEqual(1, sum_nested([1]))
        self.assertEqual(10, sum_nested([1, 2, 3, 4]))
        self.assertEqual(55, sum_nested(list(range(11))))

    def test_simple_nesting(self):
        self.assertEqual(1, sum_nested([[1]]))
        self.assertEqual(1, sum_nested([[1], []]))
        self.assertEqual(10, sum_nested([[1, 2, 3, 4]]))

    def test_complex_nesting(self):
        self.assertEqual(sum_nested([1, [1], [[1]], [[[1]]]]), 4)
        self.assertEqual(sum_nested([1, [1], [1, [1]], [1, [1], [1, [1]]]]), 8)
        self.assertEqual(0, sum_nested([[[[], [], [[[[[[[[[[]]]]]]]]]]], [], [], [[[], [[]]]]], []]))