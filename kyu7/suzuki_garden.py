# def rake_garden(garden):
# replaced = []
# for word in garden.split(" "):
# 		if word != "rock":
# 			replaced.append("gravel")
# 		else:
# 			replaced.append(word)
# 	return str.join(" ", replaced)


def rake_garden(garden):
	return " ".join(["gravel" if word != "rock" else word for word in garden.split(" ")])
